EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R2
U 1 1 5F989DB1
P 775 6275
F 0 "R2" H 845 6321 50  0000 L CNN
F 1 "4.7K" H 845 6230 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" V 705 6275 50  0001 C CNN
F 3 "~" H 775 6275 50  0001 C CNN
	1    775  6275
	1    0    0    -1  
$EndComp
Text GLabel 1150 6450 0    50   Input ~ 0
SCL
$Comp
L power:+3V3 #PWR04
U 1 1 5F98B87C
P 775 6125
F 0 "#PWR04" H 775 5975 50  0001 C CNN
F 1 "+3V3" H 790 6298 50  0000 C CNN
F 2 "" H 775 6125 50  0001 C CNN
F 3 "" H 775 6125 50  0001 C CNN
	1    775  6125
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 5F98BC9A
P 1150 6300
F 0 "R4" H 1220 6346 50  0000 L CNN
F 1 "4.7K" H 1220 6255 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" V 1080 6300 50  0001 C CNN
F 3 "~" H 1150 6300 50  0001 C CNN
	1    1150 6300
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR06
U 1 1 5F98C06B
P 1150 6150
F 0 "#PWR06" H 1150 6000 50  0001 C CNN
F 1 "+3V3" H 1165 6323 50  0000 C CNN
F 2 "" H 1150 6150 50  0001 C CNN
F 3 "" H 1150 6150 50  0001 C CNN
	1    1150 6150
	1    0    0    -1  
$EndComp
Text GLabel 775  6425 0    50   Input ~ 0
SDA
$Comp
L Mechanical:MountingHole H2
U 1 1 5F9CF5FF
P 6600 2875
F 0 "H2" H 6700 2921 50  0000 L CNN
F 1 "MountingHole" H 6700 2830 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.5mm" H 6600 2875 50  0001 C CNN
F 3 "~" H 6600 2875 50  0001 C CNN
	1    6600 2875
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x07_Female J8
U 1 1 5FB57B67
P 5825 1575
F 0 "J8" H 5853 1601 50  0000 L CNN
F 1 "SCD30" H 5853 1510 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x07_P2.54mm_Vertical" H 5825 1575 50  0001 C CNN
F 3 "~" H 5825 1575 50  0001 C CNN
	1    5825 1575
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR023
U 1 1 5FBA156B
P 5625 1275
F 0 "#PWR023" H 5625 1125 50  0001 C CNN
F 1 "+3V3" H 5640 1448 50  0000 C CNN
F 2 "" H 5625 1275 50  0001 C CNN
F 3 "" H 5625 1275 50  0001 C CNN
	1    5625 1275
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR020
U 1 1 5FBA258D
P 5275 1375
F 0 "#PWR020" H 5275 1125 50  0001 C CNN
F 1 "GND" H 5280 1202 50  0000 C CNN
F 2 "" H 5275 1375 50  0001 C CNN
F 3 "" H 5275 1375 50  0001 C CNN
	1    5275 1375
	1    0    0    -1  
$EndComp
Wire Wire Line
	5625 1375 5275 1375
Text GLabel 5625 1475 0    50   Input ~ 0
SCL
Text GLabel 5625 1575 0    50   Input ~ 0
SDA
Text Notes 1175 5650 0    79   ~ 0
i2c pullups
Text Notes 5450 1000 0    79   ~ 0
SCD30
$Comp
L Mechanical:MountingHole H1
U 1 1 5FDF5FDF
P 6600 2650
F 0 "H1" H 6700 2696 50  0000 L CNN
F 1 "MountingHole" H 6700 2605 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.7mm" H 6600 2650 50  0001 C CNN
F 3 "~" H 6600 2650 50  0001 C CNN
	1    6600 2650
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Female J10
U 1 1 60000D99
P 8325 6025
F 0 "J10" H 8353 6001 50  0000 L CNN
F 1 "ssd1306" H 8353 5910 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Vertical" H 8325 6025 50  0001 C CNN
F 3 "~" H 8325 6025 50  0001 C CNN
	1    8325 6025
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR027
U 1 1 60000DA1
P 7750 5925
F 0 "#PWR027" H 7750 5675 50  0001 C CNN
F 1 "GND" H 7755 5752 50  0000 C CNN
F 2 "" H 7750 5925 50  0001 C CNN
F 3 "" H 7750 5925 50  0001 C CNN
	1    7750 5925
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR028
U 1 1 60000DA7
P 8000 5850
F 0 "#PWR028" H 8000 5700 50  0001 C CNN
F 1 "+3V3" V 8015 5978 50  0000 L CNN
F 2 "" H 8000 5850 50  0001 C CNN
F 3 "" H 8000 5850 50  0001 C CNN
	1    8000 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	8125 6025 8000 6025
Wire Wire Line
	8000 6025 8000 5850
Wire Wire Line
	8125 5925 7750 5925
$Comp
L Switch:SW_Push_Dual SW1
U 1 1 6028B8CB
P 5400 4500
F 0 "SW1" H 5400 4785 50  0000 C CNN
F 1 "SW_Push_Dual" H 5400 4694 50  0000 C CNN
F 2 "Button_Switch_THT:SW_TH_Tactile_Omron_B3F-10xx" H 5400 4700 50  0001 C CNN
F 3 "~" H 5400 4700 50  0001 C CNN
	1    5400 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 4500 5600 4300
Wire Wire Line
	5600 4300 5200 4300
Wire Wire Line
	5200 4300 5200 4500
Wire Wire Line
	5600 4700 5600 4550
Wire Wire Line
	5600 4550 5200 4550
Wire Wire Line
	5200 4550 5200 4700
$Comp
L power:GND #PWR019
U 1 1 6028DB07
P 5600 4700
F 0 "#PWR019" H 5600 4450 50  0001 C CNN
F 1 "GND" H 5605 4527 50  0000 C CNN
F 2 "" H 5600 4700 50  0001 C CNN
F 3 "" H 5600 4700 50  0001 C CNN
	1    5600 4700
	1    0    0    -1  
$EndComp
Connection ~ 5600 4700
$Comp
L Device:R R6
U 1 1 6028DEF2
P 4950 4350
F 0 "R6" H 5020 4396 50  0000 L CNN
F 1 "4.7K" H 5020 4305 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" V 4880 4350 50  0001 C CNN
F 3 "~" H 4950 4350 50  0001 C CNN
	1    4950 4350
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR018
U 1 1 6028E7B5
P 4950 4200
F 0 "#PWR018" H 4950 4050 50  0001 C CNN
F 1 "+3V3" H 4965 4373 50  0000 C CNN
F 2 "" H 4950 4200 50  0001 C CNN
F 3 "" H 4950 4200 50  0001 C CNN
	1    4950 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 4500 4950 4500
Connection ~ 5200 4500
Text Notes 5050 3950 0    79   ~ 0
BUTTON A
$Comp
L power:GND #PWR07
U 1 1 601B48F9
P 2500 6550
F 0 "#PWR07" H 2500 6300 50  0001 C CNN
F 1 "GND" H 2505 6377 50  0000 C CNN
F 2 "" H 2500 6550 50  0001 C CNN
F 3 "" H 2500 6550 50  0001 C CNN
	1    2500 6550
	0    1    1    0   
$EndComp
Text Notes 2975 6375 0    79   ~ 0
POWER
$Comp
L Device:R R5
U 1 1 602D018E
P 4600 4350
F 0 "R5" H 4670 4396 50  0000 L CNN
F 1 "4.7K" H 4670 4305 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4530 4350 50  0001 C CNN
F 3 "~" H 4600 4350 50  0001 C CNN
	1    4600 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 4500 4600 4500
Wire Wire Line
	4600 4500 4950 4500
Connection ~ 4600 4500
Connection ~ 4950 4500
$Comp
L power:+3V3 #PWR017
U 1 1 602D6DC1
P 4600 4200
F 0 "#PWR017" H 4600 4050 50  0001 C CNN
F 1 "+3V3" H 4615 4373 50  0000 C CNN
F 2 "" H 4600 4200 50  0001 C CNN
F 3 "" H 4600 4200 50  0001 C CNN
	1    4600 4200
	1    0    0    -1  
$EndComp
Text GLabel 8125 6125 0    50   Input ~ 0
SCL
Text GLabel 8125 6225 0    50   Input ~ 0
SDA
$Comp
L Connector:Barrel_Jack_MountingPin J4
U 1 1 607F42DF
P 2800 6650
F 0 "J4" H 2570 6522 50  0000 R CNN
F 1 "Barrel_Jack_MountingPin" H 2570 6613 50  0000 R CNN
F 2 "Connector_BarrelJack:BarrelJack_Horizontal" H 2850 6610 50  0001 C CNN
F 3 "~" H 2850 6610 50  0001 C CNN
	1    2800 6650
	-1   0    0    1   
$EndComp
Wire Wire Line
	2500 6350 2500 6550
Wire Wire Line
	2500 6350 2800 6350
Connection ~ 2500 6550
Text GLabel 750  7150 0    50   Input ~ 0
SDA
$Comp
L power:+3V3 #PWR05
U 1 1 602C56A8
P 1125 6875
F 0 "#PWR05" H 1125 6725 50  0001 C CNN
F 1 "+3V3" H 1140 7048 50  0000 C CNN
F 2 "" H 1125 6875 50  0001 C CNN
F 3 "" H 1125 6875 50  0001 C CNN
	1    1125 6875
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 602C569E
P 1125 7025
F 0 "R3" H 1195 7071 50  0000 L CNN
F 1 "4.7K" H 1195 6980 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 1055 7025 50  0001 C CNN
F 3 "~" H 1125 7025 50  0001 C CNN
	1    1125 7025
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR03
U 1 1 602C5694
P 750 6850
F 0 "#PWR03" H 750 6700 50  0001 C CNN
F 1 "+3V3" H 765 7023 50  0000 C CNN
F 2 "" H 750 6850 50  0001 C CNN
F 3 "" H 750 6850 50  0001 C CNN
	1    750  6850
	1    0    0    -1  
$EndComp
Text GLabel 1125 7175 0    50   Input ~ 0
SCL
$Comp
L Device:R R1
U 1 1 602C5503
P 750 7000
F 0 "R1" H 820 7046 50  0000 L CNN
F 1 "4.7K" H 820 6955 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 680 7000 50  0001 C CNN
F 3 "~" H 750 7000 50  0001 C CNN
	1    750  7000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 60798E1F
P 5375 2700
F 0 "#PWR0101" H 5375 2450 50  0001 C CNN
F 1 "GND" H 5380 2527 50  0000 C CNN
F 2 "" H 5375 2700 50  0001 C CNN
F 3 "" H 5375 2700 50  0001 C CNN
	1    5375 2700
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR0102
U 1 1 60798E29
P 5600 2800
F 0 "#PWR0102" H 5600 2650 50  0001 C CNN
F 1 "+3V3" H 5615 2973 50  0000 C CNN
F 2 "" H 5600 2800 50  0001 C CNN
F 3 "" H 5600 2800 50  0001 C CNN
	1    5600 2800
	1    0    0    -1  
$EndComp
Text GLabel 5775 2900 0    50   Input ~ 0
SDA
Text GLabel 5775 3000 0    50   Input ~ 0
SCL
Wire Wire Line
	5775 2700 5375 2700
Wire Wire Line
	5775 2800 5600 2800
$Comp
L Connector:Conn_01x04_Female J11
U 1 1 6079898D
P 5975 2800
F 0 "J11" H 6003 2776 50  0000 L CNN
F 1 "Conn_01x04_Female" V 6150 2350 50  0000 L CNN
F 2 "Connector_JST:JST_SH_SM04B-SRSS-TB_1x04-1MP_P1.00mm_Horizontal" H 5975 2800 50  0001 C CNN
F 3 "~" H 5975 2800 50  0001 C CNN
	1    5975 2800
	1    0    0    -1  
$EndComp
Text Notes 5150 2450 0    79   ~ 0
QWIIC
$Comp
L mysensors_radios:RFM95HW U1
U 1 1 608564B2
P 9200 2300
F 0 "U1" H 9200 2914 40  0000 C CNN
F 1 "RFM95HW" H 9200 2838 40  0000 C CNN
F 2 "footprints:RFM95" H 9200 2300 30  0001 C CIN
F 3 "https://cdn-learn.adafruit.com/assets/assets/000/031/659/original/RFM95_96_97_98W.pdf?1460518717" H 10000 1150 60  0000 C CNN
	1    9200 2300
	1    0    0    -1  
$EndComp
Text GLabel 8650 2450 0    50   Input ~ 0
11_LORA_MOSI
Text GLabel 8650 2550 0    50   Input ~ 0
12_LORA_MISO
Text GLabel 8650 2650 0    50   Input ~ 0
13_LORA_SCK
$Comp
L power:GND #PWR0103
U 1 1 6086F1E9
P 9100 3150
F 0 "#PWR0103" H 9100 2900 50  0001 C CNN
F 1 "GND" H 9105 2977 50  0000 C CNN
F 2 "" H 9100 3150 50  0001 C CNN
F 3 "" H 9100 3150 50  0001 C CNN
	1    9100 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	9300 3150 9200 3150
Wire Wire Line
	9200 3150 9100 3150
Connection ~ 9200 3150
Connection ~ 9100 3150
Text GLabel 9750 2400 2    50   Input ~ 0
LORA_DIO1
Text GLabel 8650 2850 0    50   Input ~ 0
A3_LORA_RESET
$Comp
L device:Jumper_NO_Small JP1
U 1 1 608EF379
P 10450 1750
F 0 "JP1" H 10450 1935 50  0000 C CNN
F 1 "Jumper_NO_Small" H 10450 1844 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_TrianglePad1.0x1.5mm" H 10450 1750 50  0001 C CNN
F 3 "" H 10450 1750 50  0001 C CNN
	1    10450 1750
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x01_Female J5
U 1 1 609682ED
P 8350 1800
F 0 "J5" V 8288 1712 50  0000 R CNN
F 1 "Conn_01x01_Female" V 8197 1712 50  0000 R CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x01_P2.54mm_Vertical" H 8350 1800 50  0001 C CNN
F 3 "~" H 8350 1800 50  0001 C CNN
	1    8350 1800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8650 2150 8350 2150
Wire Wire Line
	8350 2150 8350 2000
Text GLabel 10550 1750 2    50   Input ~ 0
LORA_DIO1
$Comp
L power:+3V3 #PWR0104
U 1 1 60954B2C
P 9200 1950
F 0 "#PWR0104" H 9200 1800 50  0001 C CNN
F 1 "+3V3" V 9215 2078 50  0000 L CNN
F 2 "" H 9200 1950 50  0001 C CNN
F 3 "" H 9200 1950 50  0001 C CNN
	1    9200 1950
	1    0    0    -1  
$EndComp
Text GLabel 8650 2350 0    50   Input ~ 0
A4_LORA_CS
Text GLabel 9750 2300 2    50   Input ~ 0
A2_LORA_IRQ
$Comp
L Connector:Conn_01x14_Female J13
U 1 1 60B52275
P -4150 5450
F 0 "J13" H -4300 4600 50  0000 L CNN
F 1 "Conn_01x14_Female" H -4650 6150 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x14_P2.54mm_Vertical" H -4150 5450 50  0001 C CNN
F 3 "~" H -4150 5450 50  0001 C CNN
	1    -4150 5450
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x14_Female J12
U 1 1 60B539AE
P -2200 5300
F 0 "J12" H -2350 6050 50  0000 L CNN
F 1 "Conn_01x14_Female" H -2800 4500 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x14_P2.54mm_Vertical" H -2200 5300 50  0001 C CNN
F 3 "~" H -2200 5300 50  0001 C CNN
	1    -2200 5300
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x14_Female J14
U 1 1 60BBE697
P -4100 2500
F 0 "J14" H -4250 1650 50  0000 L CNN
F 1 "Conn_01x14_Female" H -4650 3200 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x14_P2.54mm_Vertical" H -4100 2500 50  0001 C CNN
F 3 "~" H -4100 2500 50  0001 C CNN
	1    -4100 2500
	-1   0    0    1   
$EndComp
$Comp
L Connector:Micro_SD_Card J15
U 1 1 60C464D6
P 2800 4450
F 0 "J15" H 2750 5167 50  0000 C CNN
F 1 "Micro_SD_Card" H 2750 5076 50  0000 C CNN
F 2 "footprints:DM3D-SF_outline" H 3950 4750 50  0001 C CNN
F 3 "http://katalog.we-online.de/em/datasheet/693072010801.pdf" H 2800 4450 50  0001 C CNN
	1    2800 4450
	1    0    0    -1  
$EndComp
Text Notes 7700 5500 0    79   ~ 0
SSD1306
$Comp
L Adafruit_AirLift_FeatherWing-eagle-import:74AHC1G125 IC1
U 1 1 6092B17E
P 3950 -850
F 0 "IC1" H 4000 -364 59  0000 C CNN
F 1 "74AHC1G125" H 4000 -469 59  0000 C CNN
F 2 "footprints:SOT23-5L" H 3950 -850 50  0001 C CNN
F 3 "" H 3950 -850 50  0001 C CNN
	1    3950 -850
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_AirLift_FeatherWing-eagle-import:ATECC108 EC1
U 1 1 6092C816
P 6100 -750
F 0 "EC1" H 6100 -364 59  0000 C CNN
F 1 "ATECC108" H 6100 -469 59  0000 C CNN
F 2 "footprints:SOIC8" H 6100 -750 50  0001 C CNN
F 3 "" H 6100 -750 50  0001 C CNN
	1    6100 -750
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_AirLift_FeatherWing-eagle-import:VREG_SOT23-5 U2
U 1 1 6096EEDE
P -2250 -1100
F 0 "U2" H -2250 -753 42  0000 C CNN
F 1 "VREG_SOT23-5" H -2250 -832 42  0000 C CNN
F 2 "footprints:SOT23-5" H -2250 -1100 50  0001 C CNN
F 3 "" H -2250 -1100 50  0001 C CNN
	1    -2250 -1100
	1    0    0    -1  
$EndComp
$Comp
L device:R R9
U 1 1 609939DD
P -3800 -900
F 0 "R9" H -3730 -854 50  0000 L CNN
F 1 "100k" H -3730 -945 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V -3870 -900 50  0001 C CNN
F 3 "" H -3800 -900 50  0001 C CNN
	1    -3800 -900
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_AirLift_FeatherWing-eagle-import:GND #GND0101
U 1 1 609C236D
P -3800 -650
F 0 "#GND0101" H -3800 -650 50  0001 C CNN
F 1 "GND" H -3800 -771 59  0000 C CNN
F 2 "" H -3800 -650 50  0001 C CNN
F 3 "" H -3800 -650 50  0001 C CNN
	1    -3800 -650
	1    0    0    -1  
$EndComp
Wire Wire Line
	-3250 -1200 -2900 -1200
$Comp
L device:R R10
U 1 1 609C5BDA
P -2900 -1050
F 0 "R10" H -2830 -1004 50  0000 L CNN
F 1 "100k" H -2950 -800 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V -2970 -1050 50  0001 C CNN
F 3 "" H -2900 -1050 50  0001 C CNN
	1    -2900 -1050
	1    0    0    -1  
$EndComp
Connection ~ -2900 -1200
Wire Wire Line
	-2900 -1200 -2650 -1200
$Comp
L Adafruit_AirLift_FeatherWing-eagle-import:GND #GND0102
U 1 1 609C64B5
P -2650 -650
F 0 "#GND0102" H -2650 -650 50  0001 C CNN
F 1 "GND" H -2650 -771 59  0000 C CNN
F 2 "" H -2650 -650 50  0001 C CNN
F 3 "" H -2650 -650 50  0001 C CNN
	1    -2650 -650
	1    0    0    -1  
$EndComp
Wire Wire Line
	-2650 -1000 -2650 -750
Wire Wire Line
	-2650 -1100 -2750 -1100
Wire Wire Line
	-2750 -1100 -2750 -800
Wire Wire Line
	-2750 -800 -2900 -800
Wire Wire Line
	-2900 -800 -2900 -900
Wire Wire Line
	-1850 -1200 -1700 -1200
Wire Wire Line
	-1700 -1200 -1700 -1500
Text GLabel -1700 -1500 2    50   Input ~ 0
3VESP
Text GLabel 13100 4200 2    50   Input ~ 0
3VESP
$Comp
L power:GND #PWR0105
U 1 1 609FA1E8
P 13100 6900
F 0 "#PWR0105" H 13100 6650 50  0001 C CNN
F 1 "GND" H 13105 6727 50  0000 C CNN
F 2 "" H 13100 6900 50  0001 C CNN
F 3 "" H 13100 6900 50  0001 C CNN
	1    13100 6900
	1    0    0    -1  
$EndComp
Text GLabel 12200 6400 0    50   Input ~ 0
ESP_RX
Text GLabel 12200 6500 0    50   Input ~ 0
ESP_TX
Text Notes -2550 -1750 0    79   ~ 0
AP2112-3.3
$Comp
L Adafruit_AirLift_FeatherWing-eagle-import:GND #GND0103
U 1 1 60A52B6F
P -3250 -800
F 0 "#GND0103" H -3250 -800 50  0001 C CNN
F 1 "GND" H -3250 -921 59  0000 C CNN
F 2 "" H -3250 -800 50  0001 C CNN
F 3 "" H -3250 -800 50  0001 C CNN
	1    -3250 -800
	1    0    0    -1  
$EndComp
$Comp
L device:C C3
U 1 1 60A5B0F4
P -1700 -1050
F 0 "C3" H -1585 -1004 50  0000 L CNN
F 1 "10u" H -1585 -1095 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H -1662 -1200 50  0001 C CNN
F 3 "" H -1700 -1050 50  0001 C CNN
	1    -1700 -1050
	1    0    0    -1  
$EndComp
Connection ~ -1700 -1200
$Comp
L Adafruit_AirLift_FeatherWing-eagle-import:GND #GND0104
U 1 1 60A5BB15
P -1700 -800
F 0 "#GND0104" H -1700 -800 50  0001 C CNN
F 1 "GND" H -1700 -921 59  0000 C CNN
F 2 "" H -1700 -800 50  0001 C CNN
F 3 "" H -1700 -800 50  0001 C CNN
	1    -1700 -800
	1    0    0    -1  
$EndComp
$Comp
L device:C C4
U 1 1 60A5C4A3
P -1250 -1050
F 0 "C4" H -1135 -1004 50  0000 L CNN
F 1 "10u" H -1135 -1095 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H -1212 -1200 50  0001 C CNN
F 3 "" H -1250 -1050 50  0001 C CNN
	1    -1250 -1050
	1    0    0    -1  
$EndComp
Text GLabel -1250 -1200 2    50   Input ~ 0
3VESP
$Comp
L Adafruit_AirLift_FeatherWing-eagle-import:GND #GND0105
U 1 1 60A5D5F4
P -1250 -800
F 0 "#GND0105" H -1250 -800 50  0001 C CNN
F 1 "GND" H -1250 -921 59  0000 C CNN
F 2 "" H -1250 -800 50  0001 C CNN
F 3 "" H -1250 -800 50  0001 C CNN
	1    -1250 -800
	1    0    0    -1  
$EndComp
$Comp
L device:R R11
U 1 1 60A8D100
P 14900 4200
F 0 "R11" H 14970 4246 50  0000 L CNN
F 1 "10K" H 14970 4155 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 14830 4200 50  0001 C CNN
F 3 "" H 14900 4200 50  0001 C CNN
	1    14900 4200
	1    0    0    -1  
$EndComp
Text GLabel 14900 3850 2    50   Input ~ 0
3VESP
Wire Wire Line
	14900 4050 14900 3850
Text GLabel -2400 5000 0    50   Input ~ 0
10_ESP_CS
Text GLabel -2400 5300 0    50   Input ~ 0
2_ESP_GPIO0
Text GLabel -2400 5900 0    50   Input ~ 0
ESP_RX
Text GLabel -2400 6000 0    50   Input ~ 0
EXP_TX
Text GLabel -3950 5950 2    50   Input ~ 0
MISO
Text GLabel -3950 5850 2    50   Input ~ 0
MOSI
Text GLabel -3950 5750 2    50   Input ~ 0
SCK
Wire Wire Line
	-3950 4850 -3550 4850
Wire Wire Line
	-3550 4850 -3550 4750
$Comp
L Adafruit_AirLift_FeatherWing-eagle-import:GND #GND0107
U 1 1 60AD7DBB
P -3000 4900
F 0 "#GND0107" H -3000 4900 50  0001 C CNN
F 1 "GND" H -3000 4779 59  0000 C CNN
F 2 "" H -3000 4900 50  0001 C CNN
F 3 "" H -3000 4900 50  0001 C CNN
	1    -3000 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	-2400 4800 -3000 4800
Text Notes -3650 4350 0    79   ~ 0
IB ESP32 ADD-ON
Text Notes -3600 950  0    79   ~ 0
IBM4
Text GLabel -3900 3000 2    50   Input ~ 0
MISO
Text GLabel -3900 2900 2    50   Input ~ 0
MOSI
Text GLabel -3900 2800 2    50   Input ~ 0
SCK
Text GLabel -3900 1800 2    50   Input ~ 0
~RESET
$Comp
L power:+3V3 #PWR0107
U 1 1 60B473C3
P -3450 1700
F 0 "#PWR0107" H -3450 1550 50  0001 C CNN
F 1 "+3V3" H -3435 1873 50  0000 C CNN
F 2 "" H -3450 1700 50  0001 C CNN
F 3 "" H -3450 1700 50  0001 C CNN
	1    -3450 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	-3900 1900 -3450 1900
Wire Wire Line
	-3450 1900 -3450 1700
Text GLabel 3550 -1050 0    50   Input ~ 0
10_ESP_CS
Text GLabel 3550 -850 0    50   Input ~ 0
ESP_MISO
$Comp
L power:GND #PWR0108
U 1 1 60B7B029
P 3550 -650
F 0 "#PWR0108" H 3550 -900 50  0001 C CNN
F 1 "GND" H 3555 -823 50  0000 C CNN
F 2 "" H 3550 -650 50  0001 C CNN
F 3 "" H 3550 -650 50  0001 C CNN
	1    3550 -650
	1    0    0    -1  
$EndComp
Text GLabel 4450 -1250 2    50   Input ~ 0
3VESP
Wire Wire Line
	4450 -1250 4450 -1050
Text GLabel 6600 -550 2    50   Input ~ 0
SDA
Text GLabel 6600 -650 2    50   Input ~ 0
SCL
$Comp
L power:GND #PWR0109
U 1 1 60B7DCF3
P 5600 -550
F 0 "#PWR0109" H 5600 -800 50  0001 C CNN
F 1 "GND" H 5605 -723 50  0000 C CNN
F 2 "" H 5600 -550 50  0001 C CNN
F 3 "" H 5600 -550 50  0001 C CNN
	1    5600 -550
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR0110
U 1 1 60B7E7D4
P 6600 -850
F 0 "#PWR0110" H 6600 -1000 50  0001 C CNN
F 1 "+3V3" H 6615 -677 50  0000 C CNN
F 2 "" H 6600 -850 50  0001 C CNN
F 3 "" H 6600 -850 50  0001 C CNN
	1    6600 -850
	1    0    0    -1  
$EndComp
$Comp
L device:C C5
U 1 1 60904288
P 2400 3250
F 0 "C5" H 2515 3296 50  0000 L CNN
F 1 "1u" H 2515 3205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2438 3100 50  0001 C CNN
F 3 "" H 2400 3250 50  0001 C CNN
	1    2400 3250
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR0116
U 1 1 60904977
P 2400 3100
F 0 "#PWR0116" H 2400 2950 50  0001 C CNN
F 1 "+3V3" H 2415 3273 50  0000 C CNN
F 2 "" H 2400 3100 50  0001 C CNN
F 3 "" H 2400 3100 50  0001 C CNN
	1    2400 3100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0117
U 1 1 609055EF
P 2400 3400
F 0 "#PWR0117" H 2400 3150 50  0001 C CNN
F 1 "GND" H 2405 3227 50  0000 C CNN
F 2 "" H 2400 3400 50  0001 C CNN
F 3 "" H 2400 3400 50  0001 C CNN
	1    2400 3400
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 5F9D006D
P 6600 3325
F 0 "H4" H 6700 3371 50  0000 L CNN
F 1 "MountingHole" H 6700 3280 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.5mm" H 6600 3325 50  0001 C CNN
F 3 "~" H 6600 3325 50  0001 C CNN
	1    6600 3325
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5F9CFBAC
P 6600 3100
F 0 "H3" H 6700 3146 50  0000 L CNN
F 1 "MountingHole" H 6700 3055 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.5mm" H 6600 3100 50  0001 C CNN
F 3 "~" H 6600 3100 50  0001 C CNN
	1    6600 3100
	1    0    0    -1  
$EndComp
Text Notes 12350 1700 0    79   ~ 0
NEOPIXEL SOLDERING
$Comp
L Connector:Conn_01x04_Female J1
U 1 1 60A7A6C9
P 13300 2500
F 0 "J1" H 13328 2476 50  0000 L CNN
F 1 "Conn_01x04_Female" H 13328 2385 50  0000 L CNN
F 2 "footprints:PinSocket_1x04_P2.54mm_Vertical_no_silk" H 13300 2500 50  0001 C CNN
F 3 "~" H 13300 2500 50  0001 C CNN
	1    13300 2500
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP1
U 1 1 60A7F8AD
P 12750 2400
F 0 "TP1" V 12945 2472 50  0000 C CNN
F 1 "TestPoint" V 12854 2472 50  0000 C CNN
F 2 "footprints:TestPoint_Pad_1.0x1.0mm_no_silk" H 12950 2400 50  0001 C CNN
F 3 "~" H 12950 2400 50  0001 C CNN
	1    12750 2400
	0    -1   -1   0   
$EndComp
$Comp
L Connector:TestPoint TP2
U 1 1 60A80254
P 12750 2500
F 0 "TP2" V 12945 2572 50  0000 C CNN
F 1 "TestPoint" V 12854 2572 50  0000 C CNN
F 2 "footprints:TestPoint_Pad_1.0x1.0mm_no_silk" H 12950 2500 50  0001 C CNN
F 3 "~" H 12950 2500 50  0001 C CNN
	1    12750 2500
	0    -1   -1   0   
$EndComp
$Comp
L Connector:TestPoint TP3
U 1 1 60A80559
P 12750 2600
F 0 "TP3" V 12945 2672 50  0000 C CNN
F 1 "TestPoint" V 12854 2672 50  0000 C CNN
F 2 "footprints:TestPoint_Pad_1.0x1.0mm_no_silk" H 12950 2600 50  0001 C CNN
F 3 "~" H 12950 2600 50  0001 C CNN
	1    12750 2600
	0    -1   -1   0   
$EndComp
$Comp
L Connector:TestPoint TP4
U 1 1 60A8079B
P 12750 2700
F 0 "TP4" V 12945 2772 50  0000 C CNN
F 1 "TestPoint" V 12854 2772 50  0000 C CNN
F 2 "footprints:TestPoint_Pad_1.0x1.0mm_no_silk" H 12950 2700 50  0001 C CNN
F 3 "~" H 12950 2700 50  0001 C CNN
	1    12750 2700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	13100 2400 12750 2400
Wire Wire Line
	13100 2600 12850 2600
Wire Wire Line
	13100 2700 12750 2700
Text GLabel -3200 1150 1    50   Input ~ 0
3_LDO_ENABLE
Text GLabel -3100 1150 1    50   Input ~ 0
4_SERDAT
$Comp
L Connector:Conn_01x05_Female J17
U 1 1 60C7C390
P -3300 1350
F 0 "J17" V -3454 1062 50  0000 R CNN
F 1 "Conn_01x05_Female" V -3363 1062 50  0000 R CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x05_P2.54mm_Vertical" H -3300 1350 50  0001 C CNN
F 3 "~" H -3300 1350 50  0001 C CNN
	1    -3300 1350
	0    -1   1    0   
$EndComp
Text Notes -4550 4900 0    79   ~ 0
IBM4
Wire Wire Line
	-3750 -1200 -3800 -1200
Connection ~ -3800 -1200
Wire Wire Line
	-3800 -1200 -3800 -1050
Wire Wire Line
	-3800 -1600 -3800 -1200
$Comp
L pspice:DIODE D1
U 1 1 6095DA07
P -3550 -1200
F 0 "D1" H -3550 -935 50  0000 C CNN
F 1 "DIODE" H -3550 -1026 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123" H -3550 -1200 50  0001 C CNN
F 3 "~" H -3550 -1200 50  0001 C CNN
	1    -3550 -1200
	1    0    0    -1  
$EndComp
Connection ~ -3250 -1200
Wire Wire Line
	-3250 -1200 -3350 -1200
$Comp
L device:C C1
U 1 1 609C2F54
P -3250 -1050
F 0 "C1" H -3135 -1004 50  0000 L CNN
F 1 "10u" H -3135 -1095 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H -3212 -1200 50  0001 C CNN
F 3 "" H -3250 -1050 50  0001 C CNN
	1    -3250 -1050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0118
U 1 1 60983EFA
P 4300 3300
F 0 "#PWR0118" H 4300 3050 50  0001 C CNN
F 1 "GND" H 4305 3127 50  0000 C CNN
F 2 "" H 4300 3300 50  0001 C CNN
F 3 "" H 4300 3300 50  0001 C CNN
	1    4300 3300
	1    0    0    -1  
$EndComp
Text GLabel 3900 3100 0    50   Input ~ 0
~RESET
Text Notes 3900 2700 0    79   ~ 0
RESET
Text GLabel -2550 2900 0    50   Input ~ 0
SDA
Text GLabel -2550 2800 0    50   Input ~ 0
SCL
Text GLabel -2550 3100 0    50   Input ~ 0
EXP_TX
Text GLabel -2550 3000 0    50   Input ~ 0
ESP_RX
$Comp
L Adafruit_AirLift_FeatherWing-eagle-import:GND #GND0108
U 1 1 60B3BEBE
P -3250 2000
F 0 "#GND0108" H -3250 2000 50  0001 C CNN
F 1 "GND" H -3250 1879 59  0000 C CNN
F 2 "" H -3250 2000 50  0001 C CNN
F 3 "" H -3250 2000 50  0001 C CNN
	1    -3250 2000
	1    0    0    -1  
$EndComp
Text GLabel -2550 1800 0    50   Input ~ 0
VBAT
Text GLabel -2550 2000 0    50   Input ~ 0
VBUS
$Comp
L Connector:Conn_01x14_Female J3
U 1 1 60BBD632
P -2350 2400
F 0 "J3" H -2500 3100 50  0000 L CNN
F 1 "Conn_01x14_Female" H -2800 1600 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x14_P2.54mm_Vertical" H -2350 2400 50  0001 C CNN
F 3 "~" H -2350 2400 50  0001 C CNN
	1    -2350 2400
	1    0    0    -1  
$EndComp
Text GLabel -3900 2300 2    50   Input ~ 0
A1_SD_CS
Text GLabel -3900 2700 2    50   Input ~ 0
A5_BUTTON_B
Text GLabel -2550 2400 0    50   Input ~ 0
10_ESP_CS
Text GLabel -2550 2500 0    50   Input ~ 0
9_ESP_BUSY
Text GLabel -2550 2600 0    50   Input ~ 0
7_ESP_RESET
$Comp
L power:GND #PWR0119
U 1 1 6097278D
P 13250 2850
F 0 "#PWR0119" H 13250 2600 50  0001 C CNN
F 1 "GND" H 13255 2677 50  0000 C CNN
F 2 "" H 13250 2850 50  0001 C CNN
F 3 "" H 13250 2850 50  0001 C CNN
	1    13250 2850
	1    0    0    -1  
$EndComp
Connection ~ 13100 2400
$Comp
L device:R R14
U 1 1 609788EA
P 12250 1850
F 0 "R14" V 12043 1850 50  0000 C CNN
F 1 "500" V 12134 1850 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" V 12180 1850 50  0001 C CNN
F 3 "" H 12250 1850 50  0001 C CNN
	1    12250 1850
	0    1    1    0   
$EndComp
Text GLabel -2550 2700 0    50   Input ~ 0
5_NEOPIXEL
Wire Wire Line
	12750 2500 13000 2500
Wire Wire Line
	13000 2500 13000 2050
Wire Wire Line
	13000 2050 12350 2050
Connection ~ 13000 2500
Wire Wire Line
	13000 2500 13100 2500
Wire Wire Line
	12850 2600 12850 1850
Connection ~ 12850 2600
Wire Wire Line
	12850 2600 12750 2600
Wire Wire Line
	12850 1850 12500 1850
Wire Wire Line
	13100 2250 13450 2250
Wire Wire Line
	13450 2250 13450 2850
Wire Wire Line
	13450 2850 13250 2850
Wire Wire Line
	13100 2850 13100 2700
Wire Wire Line
	13100 2250 13100 2400
Connection ~ 13100 2700
Connection ~ 13250 2850
Wire Wire Line
	13250 2850 13100 2850
$Comp
L device:R R15
U 1 1 6099FA2A
P 12250 1500
F 0 "R15" V 12043 1500 50  0000 C CNN
F 1 "500" V 12134 1500 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 12180 1500 50  0001 C CNN
F 3 "" H 12250 1500 50  0001 C CNN
	1    12250 1500
	0    1    1    0   
$EndComp
Wire Wire Line
	12500 1850 12500 1500
Wire Wire Line
	12500 1500 12400 1500
Connection ~ 12500 1850
Wire Wire Line
	12500 1850 12400 1850
Wire Wire Line
	12100 1500 12100 1700
Wire Wire Line
	12100 1700 11950 1700
Connection ~ 12100 1700
Wire Wire Line
	12100 1700 12100 1850
Text GLabel -2400 5200 0    50   Input ~ 0
9_ESP_BUSY
Text GLabel -2400 5100 0    50   Input ~ 0
7_ESP_RESET
Text GLabel -3900 3100 2    50   Input ~ 0
2_ESP_GPIO0
Text GLabel 11950 1700 0    50   Input ~ 0
5_NEOPIXEL
Text GLabel -3900 2200 2    50   Input ~ 0
A0_LORAWAN
Text GLabel -3550 4750 2    50   Input ~ 0
3VESP
Text GLabel 1900 4250 0    50   Input ~ 0
A1_SD_CS
Text GLabel 1900 4350 0    50   Input ~ 0
MOSI
Text GLabel 1900 4550 0    50   Input ~ 0
SCK
Text GLabel 1900 4750 0    50   Input ~ 0
MISO
$Comp
L power:GND #PWR0121
U 1 1 60AF12FC
P 1500 4650
F 0 "#PWR0121" H 1500 4400 50  0001 C CNN
F 1 "GND" H 1505 4477 50  0000 C CNN
F 2 "" H 1500 4650 50  0001 C CNN
F 3 "" H 1500 4650 50  0001 C CNN
	1    1500 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	1900 4650 1500 4650
Wire Wire Line
	1900 4450 1350 4450
Wire Wire Line
	1350 4450 1350 4150
$Comp
L power:+3V3 #PWR0122
U 1 1 60AFC4A9
P 1350 4150
F 0 "#PWR0122" H 1350 4000 50  0001 C CNN
F 1 "+3V3" H 1350 4300 50  0000 C CNN
F 2 "" H 1350 4150 50  0001 C CNN
F 3 "" H 1350 4150 50  0001 C CNN
	1    1350 4150
	1    0    0    -1  
$EndComp
Text GLabel -3900 2600 2    50   Input ~ 0
A4_LORA_CS
Text GLabel -3900 2400 2    50   Input ~ 0
A2_LORA_IRQ
Text GLabel -2550 2300 0    50   Input ~ 0
11_LORA_MOSI
Text GLabel -2550 2200 0    50   Input ~ 0
12_LORA_MISO
Text GLabel -2550 2100 0    50   Input ~ 0
13_LORA_SCK
Wire Wire Line
	-3250 1900 -2550 1900
Text GLabel 14900 4800 2    50   Input ~ 0
10_ESP_CS
Text GLabel 14200 6400 2    50   Input ~ 0
9_ESP_BUSY
Text GLabel 12200 5100 0    50   Input ~ 0
7_ESP_RESET
Text GLabel 4450 -650 2    50   Input ~ 0
MISO
Text GLabel 14200 5900 2    50   Input ~ 0
ESP_MISO
Text GLabel 14200 5100 2    50   Input ~ 0
MOSI
Text GLabel 14200 5500 2    50   Input ~ 0
SCK
Text GLabel 14200 4500 2    50   Input ~ 0
2_ESP_GPIO0
Wire Wire Line
	14200 4800 14900 4800
Wire Wire Line
	14900 4350 14900 4800
Text GLabel -3900 2500 2    50   Input ~ 0
A3_BUTTON_A
Text GLabel -3900 2000 2    50   Input ~ 0
AREF
Text GLabel -3900 2100 2    50   Input ~ 0
VHI
Text GLabel 4450 4500 3    50   Input ~ 0
A3_BUTTON_A
$Comp
L Mechanical:MountingHole H8
U 1 1 60D011E3
P 6700 1575
F 0 "H8" H 6800 1621 50  0000 L CNN
F 1 "MountingHole" H 6800 1530 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 6700 1575 50  0001 C CNN
F 3 "~" H 6700 1575 50  0001 C CNN
	1    6700 1575
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H7
U 1 1 60D011E9
P 6700 1350
F 0 "H7" H 6800 1396 50  0000 L CNN
F 1 "MountingHole" H 6800 1305 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 6700 1350 50  0001 C CNN
F 3 "~" H 6700 1350 50  0001 C CNN
	1    6700 1350
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H10
U 1 1 60D011EF
P 6700 2025
F 0 "H10" H 6800 2071 50  0000 L CNN
F 1 "MountingHole" H 6800 1980 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 6700 2025 50  0001 C CNN
F 3 "~" H 6700 2025 50  0001 C CNN
	1    6700 2025
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H9
U 1 1 60D011F5
P 6700 1800
F 0 "H9" H 6800 1846 50  0000 L CNN
F 1 "MountingHole" H 6800 1755 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 6700 1800 50  0001 C CNN
F 3 "~" H 6700 1800 50  0001 C CNN
	1    6700 1800
	1    0    0    -1  
$EndComp
Text GLabel -3800 -1600 0    50   Input ~ 0
VHI
Text GLabel -4100 100  0    50   Input ~ 0
VBAT
$Comp
L Adafruit_AirLift_FeatherWing-eagle-import:GND #GND0109
U 1 1 60D7FC5C
P -4100 300
F 0 "#GND0109" H -4100 300 50  0001 C CNN
F 1 "GND" H -4100 179 59  0000 C CNN
F 2 "" H -4100 300 50  0001 C CNN
F 3 "" H -4100 300 50  0001 C CNN
	1    -4100 300 
	1    0    0    -1  
$EndComp
Text GLabel 2500 6750 0    50   Input ~ 0
VBAT
$Comp
L Connector:Screw_Terminal_01x02 J7
U 1 1 60D7B882
P -3900 200
F 0 "J7" H -3982 -125 50  0000 C CNN
F 1 "Screw_Terminal_01x02" H -3982 -34 50  0000 C CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_PT-1,5-2-5.0-H_1x02_P5.00mm_Horizontal" H -3900 200 50  0001 C CNN
F 3 "~" H -3900 200 50  0001 C CNN
	1    -3900 200 
	1    0    0    1   
$EndComp
Text GLabel 10350 1750 0    50   Input ~ 0
A0_LORAWAN
$Comp
L Switch:SW_Push_Dual SW3
U 1 1 60EFEB86
P 4100 3100
F 0 "SW3" H 4100 3385 50  0000 C CNN
F 1 "SW_Push_Dual" H 4100 3294 50  0000 C CNN
F 2 "Button_Switch_THT:SW_TH_Tactile_Omron_B3F-10xx" H 4100 3300 50  0001 C CNN
F 3 "~" H 4100 3300 50  0001 C CNN
	1    4100 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 3100 4300 3000
Wire Wire Line
	4300 3000 3900 3000
Wire Wire Line
	3900 3000 3900 3100
Wire Wire Line
	3900 3300 3900 3200
Wire Wire Line
	3900 3200 4300 3200
Wire Wire Line
	4300 3200 4300 3300
Connection ~ 4300 3300
$Comp
L device:C C6
U 1 1 60B0E10F
P -4450 4150
F 0 "C6" H -4335 4196 50  0000 L CNN
F 1 "0.1u" H -4335 4105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H -4412 4000 50  0001 C CNN
F 3 "" H -4450 4150 50  0001 C CNN
	1    -4450 4150
	1    0    0    -1  
$EndComp
Text GLabel -4450 4000 2    50   Input ~ 0
3VESP
$Comp
L Adafruit_AirLift_FeatherWing-eagle-import:GND #GND0110
U 1 1 60B0E935
P -4450 4400
F 0 "#GND0110" H -4450 4400 50  0001 C CNN
F 1 "GND" H -4450 4279 59  0000 C CNN
F 2 "" H -4450 4400 50  0001 C CNN
F 3 "" H -4450 4400 50  0001 C CNN
	1    -4450 4400
	1    0    0    -1  
$EndComp
Text GLabel 12750 3700 2    50   Input ~ 0
3VESP
$Comp
L Adafruit_AirLift_FeatherWing-eagle-import:GND #GND0106
U 1 1 60A622CA
P 12750 4100
F 0 "#GND0106" H 12750 4100 50  0001 C CNN
F 1 "GND" H 12750 3979 59  0000 C CNN
F 2 "" H 12750 4100 50  0001 C CNN
F 3 "" H 12750 4100 50  0001 C CNN
	1    12750 4100
	1    0    0    -1  
$EndComp
$Comp
L device:C C2
U 1 1 60A60C9F
P 12750 3850
F 0 "C2" H 12865 3896 50  0000 L CNN
F 1 "0.1u" H 12865 3805 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 12788 3700 50  0001 C CNN
F 3 "" H 12750 3850 50  0001 C CNN
	1    12750 3850
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_AirLift_FeatherWing-eagle-import:ESP32_WROOM32 X1
U 1 1 609DA424
P 13100 5800
F 0 "X1" H 13100 5800 50  0001 C CNN
F 1 "ESP32_WROOM32" H 13100 5800 50  0001 C CNN
F 2 "footprints:ESP32-WROOM" H 13100 5800 50  0001 C CNN
F 3 "" H 13100 5800 50  0001 C CNN
	1    13100 5800
	1    0    0    -1  
$EndComp
$Comp
L device:C C7
U 1 1 60B4045B
P -5050 4150
F 0 "C7" H -4935 4196 50  0000 L CNN
F 1 "10u" H -4935 4105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H -5012 4000 50  0001 C CNN
F 3 "" H -5050 4150 50  0001 C CNN
	1    -5050 4150
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_AirLift_FeatherWing-eagle-import:GND #GND0111
U 1 1 60B409CF
P -5050 4400
F 0 "#GND0111" H -5050 4400 50  0001 C CNN
F 1 "GND" H -5050 4279 59  0000 C CNN
F 2 "" H -5050 4400 50  0001 C CNN
F 3 "" H -5050 4400 50  0001 C CNN
	1    -5050 4400
	1    0    0    -1  
$EndComp
Text GLabel -5050 4000 2    50   Input ~ 0
3VESP
$Comp
L Mechanical:MountingHole H5
U 1 1 609D9C6D
P 13050 900
F 0 "H5" H 13150 946 50  0000 L CNN
F 1 "MountingHole" H 13150 855 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.5mm" H 13050 900 50  0001 C CNN
F 3 "~" H 13050 900 50  0001 C CNN
	1    13050 900 
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H6
U 1 1 609DA433
P 13050 1200
F 0 "H6" H 13150 1246 50  0000 L CNN
F 1 "MountingHole" H 13150 1155 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.5mm" H 13050 1200 50  0001 C CNN
F 3 "~" H 13050 1200 50  0001 C CNN
	1    13050 1200
	1    0    0    -1  
$EndComp
Text Notes 12850 650  0    79   ~ 0
Neopixel Holes
Text Notes 2500 700  0    79   ~ 0
Breakouts
$Comp
L power:+3V3 #PWR014
U 1 1 601BD700
P 2050 1700
F 0 "#PWR014" H 2050 1550 50  0001 C CNN
F 1 "+3V3" H 2065 1873 50  0000 C CNN
F 2 "" H 2050 1700 50  0001 C CNN
F 3 "" H 2050 1700 50  0001 C CNN
	1    2050 1700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR015
U 1 1 601BEB98
P 2500 1800
F 0 "#PWR015" H 2500 1550 50  0001 C CNN
F 1 "GND" H 2505 1627 50  0000 C CNN
F 2 "" H 2500 1800 50  0001 C CNN
F 3 "" H 2500 1800 50  0001 C CNN
	1    2500 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 1800 2500 1800
Text GLabel 2800 1600 0    50   Input ~ 0
VHI
Wire Wire Line
	2050 1700 2800 1700
Text GLabel 3650 9250 1    50   Input ~ 0
A5_BUTTON_B
$Comp
L Switch:SW_Coded_SH-7010 SW2
U 1 1 60D43DCF
P 700 9750
F 0 "SW2" H 757 10217 50  0000 C CNN
F 1 "SW_Coded_SH-7010" H 757 10126 50  0000 C CNN
F 2 "footprints:sd2010_dip" H 400 9300 50  0001 L CNN
F 3 "https://www.nidec-copal-electronics.com/e/catalog/switch/sh-7000.pdf" H 700 9750 50  0001 C CNN
	1    700  9750
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR0106
U 1 1 60D45AC3
P 1200 9450
F 0 "#PWR0106" H 1200 9300 50  0001 C CNN
F 1 "+3V3" H 1215 9623 50  0000 C CNN
F 2 "" H 1200 9450 50  0001 C CNN
F 3 "" H 1200 9450 50  0001 C CNN
	1    1200 9450
	1    0    0    -1  
$EndComp
Wire Wire Line
	1100 9550 1200 9550
Wire Wire Line
	1200 9550 1200 9450
$Comp
L device:R R16
U 1 1 60D4C3B1
P 1500 10100
F 0 "R16" H 1570 10146 50  0000 L CNN
F 1 "4.7K" H 1570 10055 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" V 1430 10100 50  0001 C CNN
F 3 "" H 1500 10100 50  0001 C CNN
	1    1500 10100
	1    0    0    -1  
$EndComp
$Comp
L device:R R17
U 1 1 60D4C94E
P 1800 10100
F 0 "R17" H 1870 10146 50  0000 L CNN
F 1 "4.7K" H 1870 10055 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 1730 10100 50  0001 C CNN
F 3 "" H 1800 10100 50  0001 C CNN
	1    1800 10100
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_AirLift_FeatherWing-eagle-import:GND #GND0112
U 1 1 60D4FD48
P 1500 10350
F 0 "#GND0112" H 1500 10350 50  0001 C CNN
F 1 "GND" H 1500 10229 59  0000 C CNN
F 2 "" H 1500 10350 50  0001 C CNN
F 3 "" H 1500 10350 50  0001 C CNN
	1    1500 10350
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_AirLift_FeatherWing-eagle-import:GND #GND0113
U 1 1 60D50193
P 1800 10350
F 0 "#GND0113" H 1800 10350 50  0001 C CNN
F 1 "GND" H 1800 10229 59  0000 C CNN
F 2 "" H 1800 10350 50  0001 C CNN
F 3 "" H 1800 10350 50  0001 C CNN
	1    1800 10350
	1    0    0    -1  
$EndComp
Wire Wire Line
	1100 9950 1500 9950
Connection ~ 1500 9950
Wire Wire Line
	1500 9950 1500 9450
Text GLabel 1500 9450 1    50   Input ~ 0
A0_LORAWAN
Wire Wire Line
	1100 9850 2250 9850
Wire Wire Line
	1500 9950 1800 9950
$Comp
L device:R R18
U 1 1 60D828FE
P 2250 10000
F 0 "R18" H 2320 10046 50  0000 L CNN
F 1 "4.7K" H 2320 9955 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" V 2180 10000 50  0001 C CNN
F 3 "" H 2250 10000 50  0001 C CNN
	1    2250 10000
	1    0    0    -1  
$EndComp
$Comp
L device:R R19
U 1 1 60D82904
P 2550 10000
F 0 "R19" H 2620 10046 50  0000 L CNN
F 1 "4.7K" H 2620 9955 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 2480 10000 50  0001 C CNN
F 3 "" H 2550 10000 50  0001 C CNN
	1    2550 10000
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_AirLift_FeatherWing-eagle-import:GND #GND0114
U 1 1 60D8290A
P 2250 10250
F 0 "#GND0114" H 2250 10250 50  0001 C CNN
F 1 "GND" H 2250 10129 59  0000 C CNN
F 2 "" H 2250 10250 50  0001 C CNN
F 3 "" H 2250 10250 50  0001 C CNN
	1    2250 10250
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_AirLift_FeatherWing-eagle-import:GND #GND0115
U 1 1 60D82910
P 2550 10250
F 0 "#GND0115" H 2550 10250 50  0001 C CNN
F 1 "GND" H 2550 10129 59  0000 C CNN
F 2 "" H 2550 10250 50  0001 C CNN
F 3 "" H 2550 10250 50  0001 C CNN
	1    2550 10250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 9850 2550 9850
Connection ~ 2250 9850
Text GLabel 2250 9450 1    50   Input ~ 0
3_LDO_ENABLE
Wire Wire Line
	2250 9850 2250 9450
$Comp
L device:R R20
U 1 1 60D9AF4B
P 3000 9900
F 0 "R20" H 3070 9946 50  0000 L CNN
F 1 "4.7K" H 3070 9855 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" V 2930 9900 50  0001 C CNN
F 3 "" H 3000 9900 50  0001 C CNN
	1    3000 9900
	1    0    0    -1  
$EndComp
$Comp
L device:R R21
U 1 1 60D9AF51
P 3300 9900
F 0 "R21" H 3370 9946 50  0000 L CNN
F 1 "4.7K" H 3370 9855 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3230 9900 50  0001 C CNN
F 3 "" H 3300 9900 50  0001 C CNN
	1    3300 9900
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_AirLift_FeatherWing-eagle-import:GND #GND0116
U 1 1 60D9AF57
P 3000 10150
F 0 "#GND0116" H 3000 10150 50  0001 C CNN
F 1 "GND" H 3000 10029 59  0000 C CNN
F 2 "" H 3000 10150 50  0001 C CNN
F 3 "" H 3000 10150 50  0001 C CNN
	1    3000 10150
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_AirLift_FeatherWing-eagle-import:GND #GND0117
U 1 1 60D9AF5D
P 3300 10150
F 0 "#GND0117" H 3300 10150 50  0001 C CNN
F 1 "GND" H 3300 10029 59  0000 C CNN
F 2 "" H 3300 10150 50  0001 C CNN
F 3 "" H 3300 10150 50  0001 C CNN
	1    3300 10150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 9750 3300 9750
Connection ~ 3000 9750
Wire Wire Line
	3000 9750 3000 9350
Wire Wire Line
	3000 9750 1100 9750
Text GLabel 3000 9350 1    50   Input ~ 0
4_SERDAT
$Comp
L device:R R22
U 1 1 60DB58AA
P 3650 9800
F 0 "R22" H 3720 9846 50  0000 L CNN
F 1 "4.7K" H 3720 9755 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" V 3580 9800 50  0001 C CNN
F 3 "" H 3650 9800 50  0001 C CNN
	1    3650 9800
	1    0    0    -1  
$EndComp
$Comp
L device:R R23
U 1 1 60DB58B0
P 3950 9800
F 0 "R23" H 4020 9846 50  0000 L CNN
F 1 "4.7K" H 4020 9755 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3880 9800 50  0001 C CNN
F 3 "" H 3950 9800 50  0001 C CNN
	1    3950 9800
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_AirLift_FeatherWing-eagle-import:GND #GND0118
U 1 1 60DB58B6
P 3650 10050
F 0 "#GND0118" H 3650 10050 50  0001 C CNN
F 1 "GND" H 3650 9929 59  0000 C CNN
F 2 "" H 3650 10050 50  0001 C CNN
F 3 "" H 3650 10050 50  0001 C CNN
	1    3650 10050
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_AirLift_FeatherWing-eagle-import:GND #GND0119
U 1 1 60DB58BC
P 3950 10050
F 0 "#GND0119" H 3950 10050 50  0001 C CNN
F 1 "GND" H 3950 9929 59  0000 C CNN
F 2 "" H 3950 10050 50  0001 C CNN
F 3 "" H 3950 10050 50  0001 C CNN
	1    3950 10050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 9650 3950 9650
Connection ~ 3650 9650
Wire Wire Line
	3650 9650 3650 9250
Wire Wire Line
	1100 9650 3650 9650
Text GLabel 2800 1200 0    50   Input ~ 0
SDA
Text GLabel 2800 1300 0    50   Input ~ 0
SCL
Text GLabel 2800 1400 0    50   Input ~ 0
EXP_TX
$Comp
L Connector:Conn_01x07_Female J2
U 1 1 60E65CC9
P 3000 1500
F 0 "J2" H 3028 1526 50  0000 L CNN
F 1 "Conn_01x07_Female" H 3028 1435 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x07_P2.54mm_Vertical" H 3000 1500 50  0001 C CNN
F 3 "~" H 3000 1500 50  0001 C CNN
	1    3000 1500
	1    0    0    -1  
$EndComp
Text GLabel 2800 1500 0    50   Input ~ 0
ESP_RX
$Comp
L power:GND #PWR0111
U 1 1 60F802F8
P 6625 4550
F 0 "#PWR0111" H 6625 4300 50  0001 C CNN
F 1 "GND" H 6630 4377 50  0000 C CNN
F 2 "" H 6625 4550 50  0001 C CNN
F 3 "" H 6625 4550 50  0001 C CNN
	1    6625 4550
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR0112
U 1 1 60F802FE
P 6850 4650
F 0 "#PWR0112" H 6850 4500 50  0001 C CNN
F 1 "+3V3" H 6865 4823 50  0000 C CNN
F 2 "" H 6850 4650 50  0001 C CNN
F 3 "" H 6850 4650 50  0001 C CNN
	1    6850 4650
	1    0    0    -1  
$EndComp
Text GLabel 7025 4750 0    50   Input ~ 0
SDA
Text GLabel 7025 4850 0    50   Input ~ 0
SCL
Wire Wire Line
	7025 4550 6625 4550
Wire Wire Line
	7025 4650 6850 4650
$Comp
L Connector:Conn_01x04_Female J6
U 1 1 60F80308
P 7225 4650
F 0 "J6" H 7253 4626 50  0000 L CNN
F 1 "Conn_01x04_Female" V 7400 4200 50  0000 L CNN
F 2 "Connector_JST:JST_SH_SM04B-SRSS-TB_1x04-1MP_P1.00mm_Horizontal" H 7225 4650 50  0001 C CNN
F 3 "~" H 7225 4650 50  0001 C CNN
	1    7225 4650
	1    0    0    -1  
$EndComp
Text Notes 6400 4300 0    79   ~ 0
QWIIC
Text GLabel 12350 2050 0    50   Input ~ 0
VHI
NoConn ~ -2250 5250
NoConn ~ -4950 2850
NoConn ~ 725  3875
NoConn ~ -12025 2750
$EndSCHEMATC
