EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "PVOS CO2 Monitor"
Date ""
Rev "V"
Comp ""
Comment1 "http://pvos.org/co2/rev_v"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R2
U 1 1 5F989DB1
P 2925 4050
F 0 "R2" H 2995 4096 50  0000 L CNN
F 1 "4.7K" H 2995 4005 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" V 2855 4050 50  0001 C CNN
F 3 "~" H 2925 4050 50  0001 C CNN
	1    2925 4050
	1    0    0    -1  
$EndComp
Text GLabel 3300 4225 0    50   Input ~ 0
SCL
$Comp
L power:+3V3 #PWR016
U 1 1 5F98B87C
P 2925 3900
F 0 "#PWR016" H 2925 3750 50  0001 C CNN
F 1 "+3V3" H 2940 4073 50  0000 C CNN
F 2 "" H 2925 3900 50  0001 C CNN
F 3 "" H 2925 3900 50  0001 C CNN
	1    2925 3900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5F98BC9A
P 3300 4075
F 0 "R3" H 3370 4121 50  0000 L CNN
F 1 "4.7K" H 3370 4030 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" V 3230 4075 50  0001 C CNN
F 3 "~" H 3300 4075 50  0001 C CNN
	1    3300 4075
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR017
U 1 1 5F98C06B
P 3300 3925
F 0 "#PWR017" H 3300 3775 50  0001 C CNN
F 1 "+3V3" H 3315 4098 50  0000 C CNN
F 2 "" H 3300 3925 50  0001 C CNN
F 3 "" H 3300 3925 50  0001 C CNN
	1    3300 3925
	1    0    0    -1  
$EndComp
Text GLabel 2925 4200 0    50   Input ~ 0
SDA
Text Notes 2800 3525 0    79   ~ 0
i2c pullups
$Comp
L power:+3V3 #PWR015
U 1 1 6028E7B5
P 1125 3800
F 0 "#PWR015" H 1125 3650 50  0001 C CNN
F 1 "+3V3" H 1140 3973 50  0000 C CNN
F 2 "" H 1125 3800 50  0001 C CNN
F 3 "" H 1125 3800 50  0001 C CNN
	1    1125 3800
	1    0    0    -1  
$EndComp
Text Notes 1225 3550 0    79   ~ 0
BUTTON A
$Comp
L power:GND #PWR025
U 1 1 601B48F9
P 4850 6725
F 0 "#PWR025" H 4850 6475 50  0001 C CNN
F 1 "GND" H 4855 6552 50  0000 C CNN
F 2 "" H 4850 6725 50  0001 C CNN
F 3 "" H 4850 6725 50  0001 C CNN
	1    4850 6725
	0    1    1    0   
$EndComp
Text Notes 4200 5850 0    79   ~ 0
POWER
$Comp
L Connector:Barrel_Jack_MountingPin J10
U 1 1 607F42DF
P 5150 6825
F 0 "J10" H 4920 6697 50  0000 R CNN
F 1 "Barrel_Jack_MountingPin" H 4850 6950 50  0000 R CNN
F 2 "Connector_BarrelJack:BarrelJack_Horizontal" H 5200 6785 50  0001 C CNN
F 3 "~" H 5200 6785 50  0001 C CNN
	1    5150 6825
	-1   0    0    1   
$EndComp
Wire Wire Line
	4850 6525 4850 6725
Wire Wire Line
	4850 6525 5150 6525
Connection ~ 4850 6725
Text Notes 2750 900  0    79   ~ 0
QWIIC (i2c)
$Comp
L power:+3V3 #PWR019
U 1 1 60FBA6C6
P 1150 4875
F 0 "#PWR019" H 1150 4725 50  0001 C CNN
F 1 "+3V3" H 1165 5048 50  0000 C CNN
F 2 "" H 1150 4875 50  0001 C CNN
F 3 "" H 1150 4875 50  0001 C CNN
	1    1150 4875
	1    0    0    -1  
$EndComp
Text Notes 1250 4625 0    79   ~ 0
BUTTON B
$Comp
L Connector:Conn_01x03_Female J9
U 1 1 61020F33
P 3650 6775
F 0 "J9" H 3678 6801 50  0000 L CNN
F 1 "Conn_01x03_Female" H 3678 6710 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x03_P2.54mm_Vertical" H 3650 6775 50  0001 C CNN
F 3 "~" H 3650 6775 50  0001 C CNN
	1    3650 6775
	1    0    0    -1  
$EndComp
Text Notes 3200 6500 0    79   ~ 0
5V DC-DC
$Comp
L device:C C2
U 1 1 61051461
P 3750 7325
F 0 "C2" H 3635 7279 50  0000 R CNN
F 1 "22uF" H 3635 7370 50  0000 R CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 3788 7175 50  0001 C CNN
F 3 "" H 3750 7325 50  0001 C CNN
	1    3750 7325
	1    0    0    -1  
$EndComp
Text GLabel 3450 6875 0    50   UnSpc ~ 0
VOUT_DCDC
$Comp
L power:GND #PWR026
U 1 1 6106310F
P 2900 6775
F 0 "#PWR026" H 2900 6525 50  0001 C CNN
F 1 "GND" H 2905 6602 50  0000 C CNN
F 2 "" H 2900 6775 50  0001 C CNN
F 3 "" H 2900 6775 50  0001 C CNN
	1    2900 6775
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 6775 2900 6775
$Comp
L power:GND #PWR028
U 1 1 61125937
P 3250 7475
F 0 "#PWR028" H 3250 7225 50  0001 C CNN
F 1 "GND" H 3255 7302 50  0000 C CNN
F 2 "" H 3250 7475 50  0001 C CNN
F 3 "" H 3250 7475 50  0001 C CNN
	1    3250 7475
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR029
U 1 1 611261CE
P 3750 7475
F 0 "#PWR029" H 3750 7225 50  0001 C CNN
F 1 "GND" H 3755 7302 50  0000 C CNN
F 2 "" H 3750 7475 50  0001 C CNN
F 3 "" H 3750 7475 50  0001 C CNN
	1    3750 7475
	1    0    0    -1  
$EndComp
Text GLabel 3250 7175 0    50   Input ~ 0
VIN_DCDC
Text GLabel 3750 7175 2    50   UnSpc ~ 0
VOUT_DCDC
$Comp
L Device:D D1
U 1 1 60F73E0C
P 3800 6175
F 0 "D1" H 3800 5958 50  0000 C CNN
F 1 "D" H 3800 6049 50  0000 C CNN
F 2 "Diode_THT:D_5W_P12.70mm_Horizontal" H 3800 6175 50  0001 C CNN
F 3 "~" H 3800 6175 50  0001 C CNN
	1    3800 6175
	-1   0    0    1   
$EndComp
Text GLabel 3450 6675 0    50   Input ~ 0
VIN_DCDC
Text GLabel 4850 6925 0    50   Input ~ 0
VIN_DCDC
Text GLabel 3650 6175 0    50   UnSpc ~ 0
VOUT_DCDC
$Comp
L Switch:SW_Push SW1
U 1 1 612C3561
P 1600 4100
F 0 "SW1" H 1600 4385 50  0000 C CNN
F 1 "SW_Push" H 1600 4294 50  0000 C CNN
F 2 "Button_Switch_THT:SW_Tactile_SPST_Angled_PTS645Vx58-2LFS" H 1600 4300 50  0001 C CNN
F 3 "~" H 1600 4300 50  0001 C CNN
	1    1600 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	1125 4100 1400 4100
$Comp
L power:GND #PWR018
U 1 1 612C4DF2
P 1800 4100
F 0 "#PWR018" H 1800 3850 50  0001 C CNN
F 1 "GND" H 1805 3927 50  0000 C CNN
F 2 "" H 1800 4100 50  0001 C CNN
F 3 "" H 1800 4100 50  0001 C CNN
	1    1800 4100
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW2
U 1 1 612CCB02
P 1600 5175
F 0 "SW2" H 1600 5460 50  0000 C CNN
F 1 "SW_Push" H 1600 5369 50  0000 C CNN
F 2 "Button_Switch_THT:SW_Tactile_SPST_Angled_PTS645Vx58-2LFS" H 1600 5375 50  0001 C CNN
F 3 "~" H 1600 5375 50  0001 C CNN
	1    1600 5175
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR021
U 1 1 612CCB08
P 1800 5175
F 0 "#PWR021" H 1800 4925 50  0001 C CNN
F 1 "GND" H 1805 5002 50  0000 C CNN
F 2 "" H 1800 5175 50  0001 C CNN
F 3 "" H 1800 5175 50  0001 C CNN
	1    1800 5175
	1    0    0    -1  
$EndComp
Wire Wire Line
	1400 5175 1150 5175
Text GLabel 900  5175 0    50   Input ~ 0
A5
Text GLabel 975  4100 0    50   Input ~ 0
A4
$Comp
L Connector:Conn_01x03_Female J8
U 1 1 614DA351
P 5150 6150
F 0 "J8" H 5178 6176 50  0000 L CNN
F 1 "Conn_01x03_Female" H 5178 6085 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x03_P2.54mm_Vertical" H 5150 6150 50  0001 C CNN
F 3 "~" H 5150 6150 50  0001 C CNN
	1    5150 6150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR024
U 1 1 614DCC4C
P 4950 6250
F 0 "#PWR024" H 4950 6000 50  0001 C CNN
F 1 "GND" H 4955 6077 50  0000 C CNN
F 2 "" H 4950 6250 50  0001 C CNN
F 3 "" H 4950 6250 50  0001 C CNN
	1    4950 6250
	0    1    1    0   
$EndComp
Text Notes 5350 6000 0    79   ~ 0
ON/OFF
$Comp
L Mechanical:MountingHole_Pad H3
U 1 1 614A5606
P 7500 6050
F 0 "H3" H 7600 6099 50  0000 L CNN
F 1 "MountingHole_Pad" H 7600 6008 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 7500 6050 50  0001 C CNN
F 3 "~" H 7500 6050 50  0001 C CNN
	1    7500 6050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H2
U 1 1 614E3462
P 8500 5700
F 0 "H2" H 8600 5749 50  0000 L CNN
F 1 "MountingHole_Pad" H 8600 5658 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 8500 5700 50  0001 C CNN
F 3 "~" H 8500 5700 50  0001 C CNN
	1    8500 5700
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H4
U 1 1 614E3468
P 8500 6050
F 0 "H4" H 8600 6099 50  0000 L CNN
F 1 "MountingHole_Pad" H 8600 6008 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 8500 6050 50  0001 C CNN
F 3 "~" H 8500 6050 50  0001 C CNN
	1    8500 6050
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x07_Female J1
U 1 1 615108B0
P 5225 1475
F 0 "J1" H 5253 1501 50  0000 L CNN
F 1 "Conn_01x07_Female" H 5253 1410 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x07_P2.54mm_Vertical" H 5225 1475 50  0001 C CNN
F 3 "~" H 5225 1475 50  0001 C CNN
	1    5225 1475
	1    0    0    -1  
$EndComp
Text Notes 4975 925  0    79   ~ 0
SCD30
$Comp
L power:+3V3 #PWR02
U 1 1 61538CDB
P 5025 1175
F 0 "#PWR02" H 5025 1025 50  0001 C CNN
F 1 "+3V3" H 5040 1348 50  0000 C CNN
F 2 "" H 5025 1175 50  0001 C CNN
F 3 "" H 5025 1175 50  0001 C CNN
	1    5025 1175
	1    0    0    -1  
$EndComp
Text GLabel 5025 1375 0    50   Input ~ 0
SCL
Text GLabel 5025 1475 0    50   Input ~ 0
SDA
$Comp
L device:C C1
U 1 1 610277F2
P 3250 7325
F 0 "C1" H 3135 7279 50  0000 R CNN
F 1 "10uF" H 3135 7370 50  0000 R CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 3288 7175 50  0001 C CNN
F 3 "" H 3250 7325 50  0001 C CNN
	1    3250 7325
	1    0    0    -1  
$EndComp
Text Notes 4750 2350 0    79   ~ 0
SCD30 'Baffles'
$Comp
L power:GND #PWR013
U 1 1 6193871C
P 925 3125
F 0 "#PWR013" H 925 2875 50  0001 C CNN
F 1 "GND" H 930 2952 50  0000 C CNN
F 2 "" H 925 3125 50  0001 C CNN
F 3 "" H 925 3125 50  0001 C CNN
	1    925  3125
	1    0    0    -1  
$EndComp
Text Notes 1100 2575 0    79   ~ 0
Write Permit\n
Text GLabel 1250 2925 0    50   Input ~ 0
A3
$Comp
L Switch:SW_Push_Dual SW3
U 1 1 61BD3A38
P 1525 7125
F 0 "SW3" H 1525 7410 50  0000 C CNN
F 1 "SW_Push_Dual" H 1525 7319 50  0000 C CNN
F 2 "Button_Switch_THT:SW_TH_Tactile_Omron_B3F-10xx" H 1525 7325 50  0001 C CNN
F 3 "~" H 1525 7325 50  0001 C CNN
	1    1525 7125
	1    0    0    -1  
$EndComp
Wire Wire Line
	1325 7125 1050 7125
Wire Wire Line
	1050 7125 1050 6825
Text GLabel 1050 6825 0    50   Input ~ 0
~RESET
Wire Wire Line
	1325 7125 1325 7000
Wire Wire Line
	1325 7000 1725 7000
Wire Wire Line
	1725 7000 1725 7125
Connection ~ 1325 7125
Wire Wire Line
	1725 7325 1725 7200
Wire Wire Line
	1725 7200 1325 7200
Wire Wire Line
	1325 7200 1325 7325
Wire Wire Line
	1725 7325 1725 7450
Connection ~ 1725 7325
$Comp
L Adafruit_AirLift_FeatherWing-eagle-import:GND #GND01
U 1 1 61BE19AA
P 1725 7550
F 0 "#GND01" H 1725 7550 50  0001 C CNN
F 1 "GND" H 1725 7429 59  0000 C CNN
F 2 "" H 1725 7550 50  0001 C CNN
F 3 "" H 1725 7550 50  0001 C CNN
	1    1725 7550
	1    0    0    -1  
$EndComp
Text Notes 1100 6675 0    79   ~ 0
EXT RESET
Text GLabel 3950 6175 2    50   Input ~ 0
VBUS
$Comp
L Device:Q_NMOS_GDS Q1
U 1 1 61C12A63
P 1450 1700
F 0 "Q1" H 1654 1746 50  0000 L CNN
F 1 "Q_NMOS_GDS" H 1654 1655 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Horizontal_TabDown" H 1650 1800 50  0001 C CNN
F 3 "~" H 1450 1700 50  0001 C CNN
	1    1450 1700
	1    0    0    -1  
$EndComp
$Comp
L Device:R R6
U 1 1 61C43F0B
P 1250 1850
F 0 "R6" H 1320 1896 50  0000 L CNN
F 1 "10K" H 1320 1805 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" V 1180 1850 50  0001 C CNN
F 3 "~" H 1250 1850 50  0001 C CNN
	1    1250 1850
	1    0    0    -1  
$EndComp
Text GLabel 1025 1700 0    50   Input ~ 0
A2
$Comp
L power:GND #PWR014
U 1 1 61D01D59
P 4975 3200
F 0 "#PWR014" H 4975 2950 50  0001 C CNN
F 1 "GND" H 4980 3027 50  0000 C CNN
F 2 "" H 4975 3200 50  0001 C CNN
F 3 "" H 4975 3200 50  0001 C CNN
	1    4975 3200
	0    1    1    0   
$EndComp
Text GLabel 4975 3300 0    50   Input ~ 0
RX_D0
Text GLabel 4975 3400 0    50   Input ~ 0
TX_D1
$Comp
L power:GND #PWR08
U 1 1 61CB8180
P 1250 2000
F 0 "#PWR08" H 1250 1750 50  0001 C CNN
F 1 "GND" H 1255 1827 50  0000 C CNN
F 2 "" H 1250 2000 50  0001 C CNN
F 3 "" H 1250 2000 50  0001 C CNN
	1    1250 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1250 2000 1550 2000
Wire Wire Line
	1550 2000 1550 1900
Connection ~ 1250 2000
Text GLabel 1550 1500 0    50   Input ~ 0
POWER_1
Wire Wire Line
	1250 1700 1025 1700
Connection ~ 1250 1700
Text GLabel 4725 1275 0    50   Input ~ 0
POWER_1
Text Notes 4000 1600 0    39   ~ 0
SCD30 is switched by Q1;\nalternatively, solder jumper\n
Text GLabel 4675 3100 0    50   Input ~ 0
POWER_1
Text GLabel 4975 3000 0    50   Input ~ 0
A1
Text GLabel 4975 2900 0    50   Input ~ 0
A0
$Comp
L power:+3V3 #PWR011
U 1 1 61E3312F
P 4775 2800
F 0 "#PWR011" H 4775 2650 50  0001 C CNN
F 1 "+3V3" H 4790 2973 50  0000 C CNN
F 2 "" H 4775 2800 50  0001 C CNN
F 3 "" H 4775 2800 50  0001 C CNN
	1    4775 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4975 2800 4775 2800
Text Notes 800  825  0    79   ~ 0
Sensor Power Switch
Wire Wire Line
	1125 4100 975  4100
Connection ~ 1125 4100
Wire Wire Line
	1150 5175 900  5175
Connection ~ 1150 5175
Wire Wire Line
	950  1100 950  1175
Wire Wire Line
	1175 1100 950  1100
Wire Wire Line
	1525 1100 1375 1100
Wire Wire Line
	1525 1225 1525 1100
$Comp
L power:GND #PWR01
U 1 1 61EFFD10
P 950 1175
F 0 "#PWR01" H 950 925 50  0001 C CNN
F 1 "GND" H 955 1002 50  0000 C CNN
F 2 "" H 950 1175 50  0001 C CNN
F 3 "" H 950 1175 50  0001 C CNN
	1    950  1175
	1    0    0    -1  
$EndComp
Text GLabel 1525 1225 2    50   Input ~ 0
POWER_1
Wire Wire Line
	4725 1275 5025 1275
Wire Wire Line
	4675 3100 4975 3100
$Comp
L Device:Jumper_NO_Small JP1
U 1 1 61EFFD06
P 1275 1100
F 0 "JP1" H 1275 1285 50  0000 C CNN
F 1 "Jumper_NO_Small" H 1275 1194 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_Pad1.0x1.5mm" H 1275 1100 50  0001 C CNN
F 3 "~" H 1275 1100 50  0001 C CNN
	1    1275 1100
	1    0    0    -1  
$EndComp
Text GLabel 4950 6150 0    50   Input ~ 0
EN
$Comp
L Device:Buzzer BZ1
U 1 1 61EE785F
P 1400 6050
F 0 "BZ1" H 1552 6079 50  0000 L CNN
F 1 "Buzzer" H 1552 5988 50  0000 L CNN
F 2 "dog:adafruit_piezo_12mm" V 1375 6150 50  0001 C CNN
F 3 "~" V 1375 6150 50  0001 C CNN
	1    1400 6050
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x07_Female J5
U 1 1 61E64D43
P 5175 3100
F 0 "J5" H 5203 3126 50  0000 L CNN
F 1 "Conn_01x07_Female" H 5203 3035 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x07_P2.54mm_Vertical" H 5175 3100 50  0001 C CNN
F 3 "~" H 5175 3100 50  0001 C CNN
	1    5175 3100
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x13_Female J6
U 1 1 61CAED36
P 5775 3225
F 0 "J6" H 5803 3251 50  0000 L CNN
F 1 "Conn_01x13_Female" H 5803 3160 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x13_P2.54mm_Vertical" H 5775 3225 50  0001 C CNN
F 3 "~" H 5775 3225 50  0001 C CNN
	1    5775 3225
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J11
U 1 1 61F163EC
P 5250 7450
F 0 "J11" H 5168 7125 50  0000 C CNN
F 1 "Screw_Terminal_01x02" H 5168 7216 50  0000 C CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MKDS-1,5-2_1x02_P5.00mm_Horizontal" H 5250 7450 50  0001 C CNN
F 3 "~" H 5250 7450 50  0001 C CNN
	1    5250 7450
	1    0    0    1   
$EndComp
Text GLabel 5050 7350 0    50   Input ~ 0
VIN_DCDC
$Comp
L power:GND #PWR027
U 1 1 61F16ED3
P 5050 7450
F 0 "#PWR027" H 5050 7200 50  0001 C CNN
F 1 "GND" H 5055 7277 50  0000 C CNN
F 2 "" H 5050 7450 50  0001 C CNN
F 3 "" H 5050 7450 50  0001 C CNN
	1    5050 7450
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR010
U 1 1 61F43F0B
P 2750 2800
F 0 "#PWR010" H 2750 2550 50  0001 C CNN
F 1 "GND" H 2755 2627 50  0000 C CNN
F 2 "" H 2750 2800 50  0001 C CNN
F 3 "" H 2750 2800 50  0001 C CNN
	1    2750 2800
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR012
U 1 1 61F4428F
P 2975 2900
F 0 "#PWR012" H 2975 2750 50  0001 C CNN
F 1 "+3V3" H 2990 3073 50  0000 C CNN
F 2 "" H 2975 2900 50  0001 C CNN
F 3 "" H 2975 2900 50  0001 C CNN
	1    2975 2900
	1    0    0    -1  
$EndComp
Text GLabel 3150 3000 0    50   Input ~ 0
SDA
Text GLabel 3150 3100 0    50   Input ~ 0
SCL
Wire Wire Line
	3150 2800 2750 2800
Wire Wire Line
	3150 2900 2975 2900
$Comp
L Connector:Conn_01x04_Female J4
U 1 1 61F4429D
P 3350 2900
F 0 "J4" H 3378 2876 50  0000 L CNN
F 1 "Conn_01x04_Female" V 3525 2450 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Vertical" H 3350 2900 50  0001 C CNN
F 3 "~" H 3350 2900 50  0001 C CNN
	1    3350 2900
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H1
U 1 1 614A1FDA
P 7500 5700
F 0 "H1" H 7600 5749 50  0000 L CNN
F 1 "MountingHole_Pad" H 7600 5658 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 7500 5700 50  0001 C CNN
F 3 "~" H 7500 5700 50  0001 C CNN
	1    7500 5700
	1    0    0    -1  
$EndComp
Text GLabel 1300 5950 0    50   Input ~ 0
D5
$Comp
L power:GND #PWR023
U 1 1 61F00025
P 1300 6150
F 0 "#PWR023" H 1300 5900 50  0001 C CNN
F 1 "GND" H 1305 5977 50  0000 C CNN
F 2 "" H 1300 6150 50  0001 C CNN
F 3 "" H 1300 6150 50  0001 C CNN
	1    1300 6150
	1    0    0    -1  
$EndComp
Text Notes 1100 5750 0    79   ~ 0
BUZZER
Text Notes 2750 2550 0    79   ~ 0
i2c breakout
Text Notes 2400 4950 0    79   ~ 0
SSD1306 OLED DISPLAY
$Comp
L Connector:Conn_01x04_Female J7
U 1 1 614FA1CB
P 3350 5250
F 0 "J7" H 3378 5226 50  0000 L CNN
F 1 "Conn_01x04_Female" V 3525 4800 50  0000 L CNN
F 2 "co2:ssd1306" H 3350 5250 50  0001 C CNN
F 3 "~" H 3350 5250 50  0001 C CNN
	1    3350 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 5250 2975 5250
Wire Wire Line
	3150 5150 2750 5150
Text GLabel 3150 5350 0    50   Input ~ 0
SCL
Text GLabel 3150 5450 0    50   Input ~ 0
SDA
$Comp
L power:+3V3 #PWR022
U 1 1 614FA1C1
P 2975 5250
F 0 "#PWR022" H 2975 5100 50  0001 C CNN
F 1 "+3V3" H 2990 5423 50  0000 C CNN
F 2 "" H 2975 5250 50  0001 C CNN
F 3 "" H 2975 5250 50  0001 C CNN
	1    2975 5250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR020
U 1 1 614FA1BB
P 2750 5150
F 0 "#PWR020" H 2750 4900 50  0001 C CNN
F 1 "GND" H 2755 4977 50  0000 C CNN
F 2 "" H 2750 5150 50  0001 C CNN
F 3 "" H 2750 5150 50  0001 C CNN
	1    2750 5150
	1    0    0    -1  
$EndComp
Text Notes 7550 5400 0    79   ~ 0
MOUNTING HOLES
Text Notes 7150 4750 0    39   ~ 0
If using Feather M0 LoRa as\nmain board, and want to use LORAWAN
Text Notes 9650 975  0    39   ~ 0
by default, D13, D12, D11, \nand D10 are all used by \nesp32 featherwing
Text Notes 9650 1300 0    79   ~ 0
Featherwing
Wire Wire Line
	9100 1850 9600 1850
Wire Wire Line
	9600 2150 9250 2150
Text GLabel 9600 3950 0    50   Input ~ 0
DW
$Comp
L power:+3V3 #PWR06
U 1 1 61ED2D90
P 9100 1850
F 0 "#PWR06" H 9100 1700 50  0001 C CNN
F 1 "+3V3" V 9115 1978 50  0000 L CNN
F 2 "" H 9100 1850 50  0001 C CNN
F 3 "" H 9100 1850 50  0001 C CNN
	1    9100 1850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR09
U 1 1 61ED2D86
P 9250 2150
F 0 "#PWR09" H 9250 1900 50  0001 C CNN
F 1 "GND" H 9255 1977 50  0000 C CNN
F 2 "" H 9250 2150 50  0001 C CNN
F 3 "" H 9250 2150 50  0001 C CNN
	1    9250 2150
	1    0    0    -1  
$EndComp
Text GLabel 9600 2000 0    50   Input ~ 0
AREF
Text GLabel 9600 1700 0    50   Input ~ 0
~RESET
Text GLabel 9600 2300 0    50   Input ~ 0
A0
Text GLabel 9600 2450 0    50   Input ~ 0
A1
Text GLabel 9600 2600 0    50   Input ~ 0
A2
Text GLabel 9600 2750 0    50   Input ~ 0
A3
Text GLabel 9600 2900 0    50   Input ~ 0
A4
Text GLabel 9600 3050 0    50   Input ~ 0
A5
Text GLabel 9600 3200 0    50   Input ~ 0
SCK
Text GLabel 9600 3350 0    50   Input ~ 0
MOSI
Text GLabel 9600 3500 0    50   Input ~ 0
MISO
Text GLabel 9600 3650 0    50   Input ~ 0
RX_D0
Text GLabel 9600 3800 0    50   Input ~ 0
TX_D1
Text GLabel 10500 3050 2    50   Input ~ 0
D5
Text GLabel 10500 2900 2    50   Input ~ 0
D6
Text GLabel 10500 3350 2    50   Input ~ 0
SDA
Text GLabel 10500 3200 2    50   Input ~ 0
SCL
Text GLabel 10500 2750 2    50   Input ~ 0
D9
Text GLabel 10500 2600 2    50   Input ~ 0
D10
Text GLabel 10500 2450 2    50   Input ~ 0
D11
Text GLabel 10500 2300 2    50   Input ~ 0
D12
Text GLabel 10500 2150 2    50   Input ~ 0
D13
Text GLabel 10500 2000 2    50   Input ~ 0
VBUS
Text GLabel 10500 1850 2    50   Input ~ 0
EN
Text GLabel 10500 1700 2    50   Input ~ 0
VBAT
$Comp
L co2:feather U2
U 1 1 61E26CE5
P 10050 2550
F 0 "U2" H 10050 3665 50  0000 C CNN
F 1 "feather" H 10050 3574 50  0000 C CNN
F 2 "footprints:feather" H 10750 3200 50  0001 C CNN
F 3 "" H 10750 3200 50  0001 C CNN
	1    10050 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 1550 7250 1550
Wire Wire Line
	7250 1850 6900 1850
$Comp
L co2:feather U1
U 1 1 61E1D616
P 7700 2250
F 0 "U1" H 7700 3365 50  0000 C CNN
F 1 "feather" H 7700 3274 50  0000 C CNN
F 2 "footprints:feather" H 8400 2900 50  0001 C CNN
F 3 "" H 8400 2900 50  0001 C CNN
	1    7700 2250
	1    0    0    -1  
$EndComp
Text GLabel 7575 4475 0    50   Input ~ 0
D6
Text Notes 7400 4200 0    79   ~ 0
LORAWAN
Text GLabel 7775 4475 2    50   Input ~ 0
DW
$Comp
L Device:Jumper_NO_Small JP2
U 1 1 61E5B905
P 7675 4475
F 0 "JP2" H 7675 4660 50  0000 C CNN
F 1 "Jumper_NO_Small" H 7675 4569 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_Pad1.0x1.5mm" H 7675 4475 50  0001 C CNN
F 3 "~" H 7675 4475 50  0001 C CNN
	1    7675 4475
	1    0    0    -1  
$EndComp
Text GLabel 7250 3650 0    50   Input ~ 0
DW
Text GLabel 8150 2750 2    50   Input ~ 0
D5
Text GLabel 8150 2600 2    50   Input ~ 0
D6
$Comp
L power:+3V3 #PWR03
U 1 1 61C48A51
P 6750 1550
F 0 "#PWR03" H 6750 1400 50  0001 C CNN
F 1 "+3V3" V 6765 1678 50  0000 L CNN
F 2 "" H 6750 1550 50  0001 C CNN
F 3 "" H 6750 1550 50  0001 C CNN
	1    6750 1550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR05
U 1 1 61C48A46
P 6900 1850
F 0 "#PWR05" H 6900 1600 50  0001 C CNN
F 1 "GND" H 6905 1677 50  0000 C CNN
F 2 "" H 6900 1850 50  0001 C CNN
F 3 "" H 6900 1850 50  0001 C CNN
	1    6900 1850
	1    0    0    -1  
$EndComp
Text GLabel 7250 1700 0    50   Input ~ 0
AREF
Text GLabel 7250 1400 0    50   Input ~ 0
~RESET
Text GLabel 7250 2000 0    50   Input ~ 0
A0
Text GLabel 7250 2150 0    50   Input ~ 0
A1
Text GLabel 7250 2300 0    50   Input ~ 0
A2
Text GLabel 7250 2450 0    50   Input ~ 0
A3
Text GLabel 7250 2600 0    50   Input ~ 0
A4
Text GLabel 7250 2750 0    50   Input ~ 0
A5
Text GLabel 7250 2900 0    50   Input ~ 0
SCK
Text GLabel 7250 3050 0    50   Input ~ 0
MOSI
Text GLabel 7250 3200 0    50   Input ~ 0
MISO
Text GLabel 7250 3350 0    50   Input ~ 0
RX_D0
Text GLabel 7250 3500 0    50   Input ~ 0
TX_D1
Text GLabel 8150 3050 2    50   Input ~ 0
SDA
Text GLabel 8150 2900 2    50   Input ~ 0
SCL
Text GLabel 8150 2450 2    50   Input ~ 0
D9
Text GLabel 8150 2300 2    50   Input ~ 0
D10
Text GLabel 8150 2150 2    50   Input ~ 0
D11
Text GLabel 8150 2000 2    50   Input ~ 0
D12
Text GLabel 8150 1850 2    50   Input ~ 0
D13
Text GLabel 8150 1700 2    50   Input ~ 0
VBUS
Text GLabel 8150 1550 2    50   Input ~ 0
EN
Text GLabel 8150 1400 2    50   Input ~ 0
VBAT
Text Notes 7450 900  0    79   ~ 0
Feather
Wire Wire Line
	1250 2825 925  2825
Wire Wire Line
	1275 2825 1250 2825
Connection ~ 1250 2825
$Comp
L Connector:Conn_01x03_Female J3
U 1 1 61938716
P 1450 2825
F 0 "J3" H 1478 2851 50  0000 L CNN
F 1 "Conn_01x03_Female" H 1478 2760 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x03_P2.54mm_Vertical" H 1450 2825 50  0001 C CNN
F 3 "~" H 1450 2825 50  0001 C CNN
	1    1450 2825
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 61C73495
P 2750 1775
F 0 "#PWR04" H 2750 1525 50  0001 C CNN
F 1 "GND" H 2755 1602 50  0000 C CNN
F 2 "" H 2750 1775 50  0001 C CNN
F 3 "" H 2750 1775 50  0001 C CNN
	1    2750 1775
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR07
U 1 1 61C7348B
P 2975 1875
F 0 "#PWR07" H 2975 1725 50  0001 C CNN
F 1 "+3V3" H 2990 2048 50  0000 C CNN
F 2 "" H 2975 1875 50  0001 C CNN
F 3 "" H 2975 1875 50  0001 C CNN
	1    2975 1875
	1    0    0    -1  
$EndComp
Text GLabel 3150 1975 0    50   Input ~ 0
SDA
Text GLabel 3150 2075 0    50   Input ~ 0
SCL
Wire Wire Line
	3150 1775 2750 1775
Wire Wire Line
	3150 1875 2975 1875
$Comp
L Connector:Conn_01x04_Female J2
U 1 1 61C7347D
P 3350 1875
F 0 "J2" H 3378 1851 50  0000 L CNN
F 1 "Conn_01x04_Female" V 3525 1425 50  0000 L CNN
F 2 "Connector_JST:JST_SH_SM04B-SRSS-TB_1x04-1MP_P1.00mm_Horizontal" H 3350 1875 50  0001 C CNN
F 3 "~" H 3350 1875 50  0001 C CNN
	1    3350 1875
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 6028DEF2
P 1150 5025
F 0 "R4" H 1220 5071 50  0000 L CNN
F 1 "4.7K" H 1220 4980 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" V 1080 5025 50  0001 C CNN
F 3 "~" H 1150 5025 50  0001 C CNN
	1    1150 5025
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 60FBA6C0
P 1125 3950
F 0 "R5" H 1195 3996 50  0000 L CNN
F 1 "4.7K" H 1195 3905 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" V 1055 3950 50  0001 C CNN
F 3 "~" H 1125 3950 50  0001 C CNN
	1    1125 3950
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 6193A6F6
P 925 2975
F 0 "R1" H 995 3021 50  0000 L CNN
F 1 "R" H 995 2930 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" V 855 2975 50  0001 C CNN
F 3 "~" H 925 2975 50  0001 C CNN
	1    925  2975
	1    0    0    -1  
$EndComp
$EndSCHEMATC
