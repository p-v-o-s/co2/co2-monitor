EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "PVOS CO2 Monitor"
Date ""
Rev "V"
Comp ""
Comment1 "http://pvos.org/co2/rev_v"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Mechanical:MountingHole_Pad H3
U 1 1 614A5606
P 7500 6050
F 0 "H3" H 7600 6099 50  0000 L CNN
F 1 "MountingHole_Pad" H 7600 6008 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 7500 6050 50  0001 C CNN
F 3 "~" H 7500 6050 50  0001 C CNN
	1    7500 6050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H2
U 1 1 614E3462
P 8500 5700
F 0 "H2" H 8600 5749 50  0000 L CNN
F 1 "MountingHole_Pad" H 8600 5658 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 8500 5700 50  0001 C CNN
F 3 "~" H 8500 5700 50  0001 C CNN
	1    8500 5700
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H4
U 1 1 614E3468
P 8500 6050
F 0 "H4" H 8600 6099 50  0000 L CNN
F 1 "MountingHole_Pad" H 8600 6008 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 8500 6050 50  0001 C CNN
F 3 "~" H 8500 6050 50  0001 C CNN
	1    8500 6050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H1
U 1 1 614A1FDA
P 7500 5700
F 0 "H1" H 7600 5749 50  0000 L CNN
F 1 "MountingHole_Pad" H 7600 5658 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 7500 5700 50  0001 C CNN
F 3 "~" H 7500 5700 50  0001 C CNN
	1    7500 5700
	1    0    0    -1  
$EndComp
Text Notes 7550 5400 0    79   ~ 0
MOUNTING HOLES
$Comp
L Mechanical:MountingHole_Pad H6
U 1 1 6242BC0C
P 7100 5250
F 0 "H6" H 7200 5299 50  0000 L CNN
F 1 "MountingHole_Pad" H 7200 5208 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 7100 5250 50  0001 C CNN
F 3 "~" H 7100 5250 50  0001 C CNN
	1    7100 5250
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H5
U 1 1 6242BC98
P 7100 4900
F 0 "H5" H 7200 4949 50  0000 L CNN
F 1 "MountingHole_Pad" H 7200 4858 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 7100 4900 50  0001 C CNN
F 3 "~" H 7100 4900 50  0001 C CNN
	1    7100 4900
	1    0    0    -1  
$EndComp
$EndSCHEMATC
