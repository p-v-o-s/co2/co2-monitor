import board
import busio
import digitalio
from digitalio import DigitalInOut
import adafruit_scd30
import time

import board
import displayio
import framebufferio
import sharpdisplay
        
# Release the existing display, if any
displayio.release_displays()

bus = board.SPI()
chip_select_pin = board.D5

framebuffer = sharpdisplay.SharpMemoryFramebuffer(bus, chip_select_pin, width=144, height=168, baudrate=8000000)

display = framebufferio.FramebufferDisplay(framebuffer)

from adafruit_display_text.label import Label
from terminalio import FONT

#fet
q1 = digitalio.DigitalInOut(board.D9)
q1.direction = digitalio.Direction.OUTPUT
q1.value = True

exception_count = 0
sample_recorded_count = 0

time_init=time.monotonic()

interval_seconds = 2
time.sleep(3)

scd = adafruit_scd30.SCD30(board.I2C())


label = Label(font=FONT, text="startup", x=10, y=10, scale=3, line_spacing=1)
display.show(label)

while True:

    if (scd.data_available):
        try:
            if (scd.CO2>1):
                # print("Data Available!")
                print("CO2: %d PPM" % scd.CO2)

                label.text= str(round(scd.CO2))
                display.show(label)


        except Exception as e:
            print("*** Exception: " + str(e))
            exception_count += 1
            # break
