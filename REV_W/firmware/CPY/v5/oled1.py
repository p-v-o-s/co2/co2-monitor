import board
import busio
import digitalio
from digitalio import DigitalInOut
import time
from adafruit_bitmap_font import bitmap_font
from adafruit_display_text import label
import microcontroller

q1 = digitalio.DigitalInOut(board.A2)
q1.direction = digitalio.Direction.OUTPUT
q1.value = True

import displayio
import terminalio
#import neopixel
from adafruit_display_text import label
import adafruit_displayio_ssd1306

displayio.release_displays()

i2c = board.I2C()
display_bus = displayio.I2CDisplay(i2c, device_address=0x3C)
display = adafruit_displayio_ssd1306.SSD1306(display_bus, width=128, height=32)

text = "PVOS.ORG\nCO2 Monitor\nREV_L"
text_area = label.Label(terminalio.FONT, text=text, color=0xFFFF00, x=20, y=15)
display.show(text_area)