EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "PVOS CO2 Monitor"
Date ""
Rev "V"
Comp ""
Comment1 "http://pvos.org/co2/rev_v"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R1
U 1 1 5F989DB1
P 975 6200
F 0 "R1" H 1045 6246 50  0000 L CNN
F 1 "4.7K" H 1045 6155 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" V 905 6200 50  0001 C CNN
F 3 "~" H 975 6200 50  0001 C CNN
	1    975  6200
	1    0    0    -1  
$EndComp
Text GLabel 1350 6375 0    50   Input ~ 0
SCL
$Comp
L power:+3V3 #PWR01
U 1 1 5F98B87C
P 975 6050
F 0 "#PWR01" H 975 5900 50  0001 C CNN
F 1 "+3V3" H 990 6223 50  0000 C CNN
F 2 "" H 975 6050 50  0001 C CNN
F 3 "" H 975 6050 50  0001 C CNN
	1    975  6050
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5F98BC9A
P 1350 6225
F 0 "R2" H 1420 6271 50  0000 L CNN
F 1 "4.7K" H 1420 6180 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" V 1280 6225 50  0001 C CNN
F 3 "~" H 1350 6225 50  0001 C CNN
	1    1350 6225
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR03
U 1 1 5F98C06B
P 1350 6075
F 0 "#PWR03" H 1350 5925 50  0001 C CNN
F 1 "+3V3" H 1365 6248 50  0000 C CNN
F 2 "" H 1350 6075 50  0001 C CNN
F 3 "" H 1350 6075 50  0001 C CNN
	1    1350 6075
	1    0    0    -1  
$EndComp
Text GLabel 975  6350 0    50   Input ~ 0
SDA
Text Notes 850  5675 0    79   ~ 0
i2c pullups
$Comp
L Device:R R3
U 1 1 6028DEF2
P 5275 5950
F 0 "R3" H 5345 5996 50  0000 L CNN
F 1 "4.7K" H 5345 5905 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" V 5205 5950 50  0001 C CNN
F 3 "~" H 5275 5950 50  0001 C CNN
	1    5275 5950
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR011
U 1 1 6028E7B5
P 5250 4725
F 0 "#PWR011" H 5250 4575 50  0001 C CNN
F 1 "+3V3" H 5265 4898 50  0000 C CNN
F 2 "" H 5250 4725 50  0001 C CNN
F 3 "" H 5250 4725 50  0001 C CNN
	1    5250 4725
	1    0    0    -1  
$EndComp
Text Notes 5350 4475 0    79   ~ 0
BUTTON A
$Comp
L power:GND #PWR010
U 1 1 601B48F9
P 3600 6625
F 0 "#PWR010" H 3600 6375 50  0001 C CNN
F 1 "GND" H 3605 6452 50  0000 C CNN
F 2 "" H 3600 6625 50  0001 C CNN
F 3 "" H 3600 6625 50  0001 C CNN
	1    3600 6625
	0    1    1    0   
$EndComp
Text Notes 3125 5350 0    79   ~ 0
POWER
$Comp
L Connector:Barrel_Jack_MountingPin J7
U 1 1 607F42DF
P 3900 6725
F 0 "J7" H 3670 6597 50  0000 R CNN
F 1 "Barrel_Jack_MountingPin" V 3475 6850 50  0000 R CNN
F 2 "Connector_BarrelJack:BarrelJack_Horizontal" H 3950 6685 50  0001 C CNN
F 3 "~" H 3950 6685 50  0001 C CNN
	1    3900 6725
	-1   0    0    1   
$EndComp
Wire Wire Line
	3600 6425 3600 6625
Wire Wire Line
	3600 6425 3900 6425
Connection ~ 3600 6625
$Comp
L Adafruit_AirLift_FeatherWing-eagle-import:GND #GND01
U 1 1 60D7FC5C
P 3800 7575
F 0 "#GND01" H 3800 7575 50  0001 C CNN
F 1 "GND" H 3800 7454 59  0000 C CNN
F 2 "" H 3800 7575 50  0001 C CNN
F 3 "" H 3800 7575 50  0001 C CNN
	1    3800 7575
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J8
U 1 1 60D7B882
P 4000 7475
F 0 "J8" H 3918 7150 50  0000 C CNN
F 1 "Screw_Terminal_01x02" H 3918 7241 50  0000 C CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_PT-1,5-2-5.0-H_1x02_P5.00mm_Horizontal" H 4000 7475 50  0001 C CNN
F 3 "~" H 4000 7475 50  0001 C CNN
	1    4000 7475
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR017
U 1 1 60F802F8
P 6625 4550
F 0 "#PWR017" H 6625 4300 50  0001 C CNN
F 1 "GND" H 6630 4377 50  0000 C CNN
F 2 "" H 6625 4550 50  0001 C CNN
F 3 "" H 6625 4550 50  0001 C CNN
	1    6625 4550
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR018
U 1 1 60F802FE
P 6850 4650
F 0 "#PWR018" H 6850 4500 50  0001 C CNN
F 1 "+3V3" H 6865 4823 50  0000 C CNN
F 2 "" H 6850 4650 50  0001 C CNN
F 3 "" H 6850 4650 50  0001 C CNN
	1    6850 4650
	1    0    0    -1  
$EndComp
Text GLabel 7025 4750 0    50   Input ~ 0
SDA
Text GLabel 7025 4850 0    50   Input ~ 0
SCL
Wire Wire Line
	7025 4550 6625 4550
Wire Wire Line
	7025 4650 6850 4650
$Comp
L Connector:Conn_01x04_Female J12
U 1 1 60F80308
P 7225 4650
F 0 "J12" H 7253 4626 50  0000 L CNN
F 1 "Conn_01x04_Female" V 7400 4200 50  0000 L CNN
F 2 "Connector_JST:JST_SH_SM04B-SRSS-TB_1x04-1MP_P1.00mm_Horizontal" H 7225 4650 50  0001 C CNN
F 3 "~" H 7225 4650 50  0001 C CNN
	1    7225 4650
	1    0    0    -1  
$EndComp
Text Notes 6400 4300 0    79   ~ 0
QWIIC
$Comp
L Device:R R4
U 1 1 60FBA6C0
P 5250 4875
F 0 "R4" H 5320 4921 50  0000 L CNN
F 1 "4.7K" H 5320 4830 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" V 5180 4875 50  0001 C CNN
F 3 "~" H 5250 4875 50  0001 C CNN
	1    5250 4875
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR013
U 1 1 60FBA6C6
P 5275 5800
F 0 "#PWR013" H 5275 5650 50  0001 C CNN
F 1 "+3V3" H 5290 5973 50  0000 C CNN
F 2 "" H 5275 5800 50  0001 C CNN
F 3 "" H 5275 5800 50  0001 C CNN
	1    5275 5800
	1    0    0    -1  
$EndComp
Text Notes 5375 5550 0    79   ~ 0
BUTTON B
Wire Wire Line
	4750 5025 5250 5025
Wire Wire Line
	4775 6100 5275 6100
$Comp
L Connector:Conn_01x03_Female J4
U 1 1 61020F33
P 2400 6675
F 0 "J4" H 2428 6701 50  0000 L CNN
F 1 "Conn_01x03_Female" H 2428 6610 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x03_P2.54mm_Vertical" H 2400 6675 50  0001 C CNN
F 3 "~" H 2400 6675 50  0001 C CNN
	1    2400 6675
	1    0    0    -1  
$EndComp
Text Notes 2100 6375 0    79   ~ 0
DC-DC
$Comp
L device:C C2
U 1 1 61051461
P 2500 7225
F 0 "C2" H 2385 7179 50  0000 R CNN
F 1 "22uF" H 2385 7270 50  0000 R CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 2538 7075 50  0001 C CNN
F 3 "" H 2500 7225 50  0001 C CNN
	1    2500 7225
	1    0    0    -1  
$EndComp
Text GLabel 2200 6775 0    50   UnSpc ~ 0
VOUT_DCDC
$Comp
L power:GND #PWR04
U 1 1 6106310F
P 1650 6675
F 0 "#PWR04" H 1650 6425 50  0001 C CNN
F 1 "GND" H 1655 6502 50  0000 C CNN
F 2 "" H 1650 6675 50  0001 C CNN
F 3 "" H 1650 6675 50  0001 C CNN
	1    1650 6675
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 6675 1650 6675
$Comp
L power:GND #PWR05
U 1 1 61125937
P 2000 7375
F 0 "#PWR05" H 2000 7125 50  0001 C CNN
F 1 "GND" H 2005 7202 50  0000 C CNN
F 2 "" H 2000 7375 50  0001 C CNN
F 3 "" H 2000 7375 50  0001 C CNN
	1    2000 7375
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR06
U 1 1 611261CE
P 2500 7375
F 0 "#PWR06" H 2500 7125 50  0001 C CNN
F 1 "GND" H 2505 7202 50  0000 C CNN
F 2 "" H 2500 7375 50  0001 C CNN
F 3 "" H 2500 7375 50  0001 C CNN
	1    2500 7375
	1    0    0    -1  
$EndComp
Text GLabel 2000 7075 0    50   Input ~ 0
VIN_DCDC
Text GLabel 2500 7075 2    50   UnSpc ~ 0
VOUT_DCDC
$Comp
L Device:D D1
U 1 1 60F73E0C
P 3250 5975
F 0 "D1" H 3250 5758 50  0000 C CNN
F 1 "D" H 3250 5849 50  0000 C CNN
F 2 "Diode_THT:D_5W_P12.70mm_Horizontal" H 3250 5975 50  0001 C CNN
F 3 "~" H 3250 5975 50  0001 C CNN
	1    3250 5975
	-1   0    0    1   
$EndComp
Text GLabel 2200 6575 0    50   Input ~ 0
VIN_DCDC
Text GLabel 3600 6825 0    50   Input ~ 0
VIN_DCDC
Text GLabel 3100 5975 0    50   UnSpc ~ 0
VOUT_DCDC
Text Notes 2750 5625 0    47   ~ 0
diode voltage drop means we should\nadd voltage to min input voltage on DC DC\neither 3.3 or 5V DC DC should work
$Comp
L Connector:Conn_01x12_Female J9
U 1 1 612D6942
P 4125 2375
F 0 "J9" H 4153 2351 50  0000 L CNN
F 1 "Conn_01x12_Female" V 4225 1475 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x12_P2.54mm_Vertical" H 4125 2375 50  0001 C CNN
F 3 "~" H 4125 2375 50  0001 C CNN
	1    4125 2375
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x16_Female J10
U 1 1 612D6948
P 4575 2700
F 0 "J10" H 4603 2676 50  0000 L CNN
F 1 "Conn_01x16_Female" V 4675 1775 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x16_P2.54mm_Vertical" H 4575 2700 50  0001 C CNN
F 3 "~" H 4575 2700 50  0001 C CNN
	1    4575 2700
	-1   0    0    1   
$EndComp
Text Notes 3825 1475 0    79   ~ 0
Airlift Featherwing
$Comp
L Connector:Conn_01x02_Female J1
U 1 1 612A5BF6
P 1475 5050
F 0 "J1" H 1503 5026 50  0000 L CNN
F 1 "Conn_01x02_Female" H 1503 4935 50  0000 L CNN
F 2 "Connector_JST:JST_PH_S2B-PH-K_1x02_P2.00mm_Horizontal" H 1475 5050 50  0001 C CNN
F 3 "~" H 1475 5050 50  0001 C CNN
	1    1475 5050
	1    0    0    -1  
$EndComp
Text Notes 1125 4900 0    79   ~ 0
JST VBat Lithium Ion
$Comp
L Switch:SW_Push SW1
U 1 1 612C3561
P 5725 5025
F 0 "SW1" H 5725 5310 50  0000 C CNN
F 1 "SW_Push" H 5725 5219 50  0000 C CNN
F 2 "Button_Switch_THT:SW_Tactile_SPST_Angled_PTS645Vx58-2LFS" H 5725 5225 50  0001 C CNN
F 3 "~" H 5725 5225 50  0001 C CNN
	1    5725 5025
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 5025 5525 5025
Connection ~ 5250 5025
$Comp
L power:GND #PWR015
U 1 1 612C4DF2
P 5925 5025
F 0 "#PWR015" H 5925 4775 50  0001 C CNN
F 1 "GND" H 5930 4852 50  0000 C CNN
F 2 "" H 5925 5025 50  0001 C CNN
F 3 "" H 5925 5025 50  0001 C CNN
	1    5925 5025
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW2
U 1 1 612CCB02
P 5725 6100
F 0 "SW2" H 5725 6385 50  0000 C CNN
F 1 "SW_Push" H 5725 6294 50  0000 C CNN
F 2 "Button_Switch_THT:SW_Tactile_SPST_Angled_PTS645Vx58-2LFS" H 5725 6300 50  0001 C CNN
F 3 "~" H 5725 6300 50  0001 C CNN
	1    5725 6100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR016
U 1 1 612CCB08
P 5925 6100
F 0 "#PWR016" H 5925 5850 50  0001 C CNN
F 1 "GND" H 5930 5927 50  0000 C CNN
F 2 "" H 5925 6100 50  0001 C CNN
F 3 "" H 5925 6100 50  0001 C CNN
	1    5925 6100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5525 6100 5275 6100
Connection ~ 5275 6100
Text GLabel 3925 1875 0    50   Input ~ 0
VBAT
Text GLabel 3925 1975 0    50   Input ~ 0
EN
Text GLabel 3925 2075 0    50   Input ~ 0
VBUS
Text GLabel 3925 2175 0    50   Input ~ 0
D13
Text GLabel 3925 2275 0    50   Input ~ 0
D12
Text GLabel 3925 2375 0    50   Input ~ 0
D11
Text GLabel 3925 2475 0    50   Input ~ 0
D10
Text GLabel 3925 2575 0    50   Input ~ 0
D9
Text GLabel 3925 2875 0    50   Input ~ 0
SCL
Text GLabel 3925 2975 0    50   Input ~ 0
SDA
Text GLabel 4775 3400 2    50   Input ~ 0
D4
Text GLabel 4775 3300 2    50   Input ~ 0
TX_D1
Text GLabel 4775 3200 2    50   Input ~ 0
RX_D0
Text GLabel 4775 3100 2    50   Input ~ 0
MISO
Text GLabel 4775 3000 2    50   Input ~ 0
MOSI
Text GLabel 4775 2900 2    50   Input ~ 0
SCK
Text GLabel 4775 2800 2    50   Input ~ 0
A5
Text GLabel 4775 2700 2    50   Input ~ 0
A4
Text GLabel 4775 2600 2    50   Input ~ 0
A3
Text GLabel 4775 2500 2    50   Input ~ 0
A2
Text GLabel 4775 2400 2    50   Input ~ 0
A1
Text GLabel 4775 2300 2    50   Input ~ 0
A0
Text GLabel 4775 1900 2    50   Input ~ 0
~RESET
Text GLabel 4775 2100 2    50   Input ~ 0
AREF
$Comp
L power:GND #PWR012
U 1 1 61347A41
P 5075 2200
F 0 "#PWR012" H 5075 1950 50  0001 C CNN
F 1 "GND" H 5080 2027 50  0000 C CNN
F 2 "" H 5075 2200 50  0001 C CNN
F 3 "" H 5075 2200 50  0001 C CNN
	1    5075 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4775 2200 5075 2200
$Comp
L power:+3V3 #PWR014
U 1 1 61347A48
P 5150 2000
F 0 "#PWR014" H 5150 1850 50  0001 C CNN
F 1 "+3V3" V 5165 2128 50  0000 L CNN
F 2 "" H 5150 2000 50  0001 C CNN
F 3 "" H 5150 2000 50  0001 C CNN
	1    5150 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4775 2000 5150 2000
Text GLabel 4775 6100 0    50   Input ~ 0
A5
Text GLabel 4750 5025 0    50   Input ~ 0
A4
$Comp
L Connector:Conn_01x03_Female J5
U 1 1 614DA351
P 4700 4125
F 0 "J5" H 4728 4151 50  0000 L CNN
F 1 "Conn_01x03_Female" H 4728 4060 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x03_P2.54mm_Vertical" H 4700 4125 50  0001 C CNN
F 3 "~" H 4700 4125 50  0001 C CNN
	1    4700 4125
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR09
U 1 1 614DCC4C
P 4500 4025
F 0 "#PWR09" H 4500 3775 50  0001 C CNN
F 1 "GND" H 4505 3852 50  0000 C CNN
F 2 "" H 4500 4025 50  0001 C CNN
F 3 "" H 4500 4025 50  0001 C CNN
	1    4500 4025
	0    1    1    0   
$EndComp
Text GLabel 4500 4225 0    50   Input ~ 0
VBAT_EXT
Text GLabel 4500 4125 0    50   Input ~ 0
VBAT
Text Notes 4350 3875 0    79   ~ 0
BATT ON/OFF
$Comp
L power:GND #PWR021
U 1 1 614FA1BB
P 8200 3700
F 0 "#PWR021" H 8200 3450 50  0001 C CNN
F 1 "GND" H 8205 3527 50  0000 C CNN
F 2 "" H 8200 3700 50  0001 C CNN
F 3 "" H 8200 3700 50  0001 C CNN
	1    8200 3700
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR022
U 1 1 614FA1C1
P 8425 3800
F 0 "#PWR022" H 8425 3650 50  0001 C CNN
F 1 "+3V3" H 8440 3973 50  0000 C CNN
F 2 "" H 8425 3800 50  0001 C CNN
F 3 "" H 8425 3800 50  0001 C CNN
	1    8425 3800
	1    0    0    -1  
$EndComp
Text GLabel 8600 4000 0    50   Input ~ 0
SDA
Text GLabel 8600 3900 0    50   Input ~ 0
SCL
Wire Wire Line
	8600 3700 8200 3700
Wire Wire Line
	8600 3800 8425 3800
$Comp
L Connector:Conn_01x04_Female J16
U 1 1 614FA1CB
P 8800 3800
F 0 "J16" H 8828 3776 50  0000 L CNN
F 1 "Conn_01x04_Female" V 8975 3350 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Vertical" H 8800 3800 50  0001 C CNN
F 3 "~" H 8800 3800 50  0001 C CNN
	1    8800 3800
	1    0    0    -1  
$EndComp
Text Notes 7975 3450 0    79   ~ 0
QWIIC
Text Notes 9475 2075 0    79   ~ 0
QWIIC
$Comp
L Mechanical:MountingHole_Pad H1
U 1 1 614A1FDA
P 7075 5825
F 0 "H1" H 7175 5874 50  0000 L CNN
F 1 "MountingHole_Pad" H 7175 5783 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5_Pad" H 7075 5825 50  0001 C CNN
F 3 "~" H 7075 5825 50  0001 C CNN
	1    7075 5825
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H2
U 1 1 614A5606
P 7325 6150
F 0 "H2" H 7425 6199 50  0000 L CNN
F 1 "MountingHole_Pad" H 7425 6108 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5_Pad" H 7325 6150 50  0001 C CNN
F 3 "~" H 7325 6150 50  0001 C CNN
	1    7325 6150
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H5
U 1 1 614E3462
P 9725 5800
F 0 "H5" H 9825 5849 50  0000 L CNN
F 1 "MountingHole_Pad" H 9825 5758 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5_Pad" H 9725 5800 50  0001 C CNN
F 3 "~" H 9725 5800 50  0001 C CNN
	1    9725 5800
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H6
U 1 1 614E3468
P 9725 6150
F 0 "H6" H 9825 6199 50  0000 L CNN
F 1 "MountingHole_Pad" H 9825 6108 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5_Pad" H 9725 6150 50  0001 C CNN
F 3 "~" H 9725 6150 50  0001 C CNN
	1    9725 6150
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x07_Female J17
U 1 1 615108B0
P 6625 2100
F 0 "J17" H 6653 2126 50  0000 L CNN
F 1 "Conn_01x07_Female" H 6653 2035 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x07_P2.54mm_Vertical" H 6625 2100 50  0001 C CNN
F 3 "~" H 6625 2100 50  0001 C CNN
	1    6625 2100
	1    0    0    -1  
$EndComp
Text Notes 6225 1525 0    79   ~ 0
SCD30
$Comp
L Connector:Conn_01x15_Female J6
U 1 1 6151B30B
P 9850 2325
F 0 "J6" V 9923 2305 50  0000 C CNN
F 1 "Conn_01x15_Female" V 10014 2305 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x15_P2.54mm_Vertical" H 9850 2325 50  0001 C CNN
F 3 "~" H 9850 2325 50  0001 C CNN
	1    9850 2325
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x07_Female J11
U 1 1 614A7D6F
P 8925 1900
F 0 "J11" H 8953 1926 50  0000 L CNN
F 1 "Conn_01x07_Female" H 8953 1835 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x07_P2.54mm_Vertical" H 8925 1900 50  0001 C CNN
F 3 "~" H 8925 1900 50  0001 C CNN
	1    8925 1900
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR0101
U 1 1 61538CDB
P 6425 1800
F 0 "#PWR0101" H 6425 1650 50  0001 C CNN
F 1 "+3V3" H 6440 1973 50  0000 C CNN
F 2 "" H 6425 1800 50  0001 C CNN
F 3 "" H 6425 1800 50  0001 C CNN
	1    6425 1800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 6153EC0E
P 6125 1900
F 0 "#PWR0102" H 6125 1650 50  0001 C CNN
F 1 "GND" H 6130 1727 50  0000 C CNN
F 2 "" H 6125 1900 50  0001 C CNN
F 3 "" H 6125 1900 50  0001 C CNN
	1    6125 1900
	1    0    0    -1  
$EndComp
Text GLabel 6425 2000 0    50   Input ~ 0
SCL
Wire Wire Line
	6125 1900 6425 1900
Text GLabel 6425 2100 0    50   Input ~ 0
SDA
$Comp
L power:+3V3 #PWR0103
U 1 1 61555B41
P 9350 1625
F 0 "#PWR0103" H 9350 1475 50  0001 C CNN
F 1 "+3V3" H 9365 1798 50  0000 C CNN
F 2 "" H 9350 1625 50  0001 C CNN
F 3 "" H 9350 1625 50  0001 C CNN
	1    9350 1625
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 61556D98
P 9650 1725
F 0 "#PWR0104" H 9650 1475 50  0001 C CNN
F 1 "GND" H 9655 1552 50  0000 C CNN
F 2 "" H 9650 1725 50  0001 C CNN
F 3 "" H 9650 1725 50  0001 C CNN
	1    9650 1725
	0    1    1    0   
$EndComp
Wire Wire Line
	9650 1625 9350 1625
$Comp
L power:GND #PWR0105
U 1 1 615C33CE
P 7075 5925
F 0 "#PWR0105" H 7075 5675 50  0001 C CNN
F 1 "GND" H 7080 5752 50  0000 C CNN
F 2 "" H 7075 5925 50  0001 C CNN
F 3 "" H 7075 5925 50  0001 C CNN
	1    7075 5925
	0    1    1    0   
$EndComp
$Comp
L power:+3V3 #PWR0106
U 1 1 615C451F
P 7325 6250
F 0 "#PWR0106" H 7325 6100 50  0001 C CNN
F 1 "+3V3" H 7340 6423 50  0000 C CNN
F 2 "" H 7325 6250 50  0001 C CNN
F 3 "" H 7325 6250 50  0001 C CNN
	1    7325 6250
	0    -1   -1   0   
$EndComp
Text GLabel 9725 6250 0    50   Input ~ 0
SCL
Text GLabel 9725 5900 0    50   Input ~ 0
SDA
$Comp
L device:C C1
U 1 1 610277F2
P 2000 7225
F 0 "C1" H 1885 7179 50  0000 R CNN
F 1 "10uF" H 1885 7270 50  0000 R CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 2038 7075 50  0001 C CNN
F 3 "" H 2000 7225 50  0001 C CNN
	1    2000 7225
	1    0    0    -1  
$EndComp
Text Notes 8175 1375 0    79   ~ 0
'Air baffle' headers for SCD30
$Comp
L power:GND #PWR0107
U 1 1 6193871C
P 2700 4475
F 0 "#PWR0107" H 2700 4225 50  0001 C CNN
F 1 "GND" H 2705 4302 50  0000 C CNN
F 2 "" H 2700 4475 50  0001 C CNN
F 3 "" H 2700 4475 50  0001 C CNN
	1    2700 4475
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 6193A6F6
P 2700 4325
F 0 "R5" H 2770 4371 50  0000 L CNN
F 1 "R" H 2770 4280 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" V 2630 4325 50  0001 C CNN
F 3 "~" H 2700 4325 50  0001 C CNN
	1    2700 4325
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x03_Female J19
U 1 1 61938716
P 2900 4075
F 0 "J19" H 2928 4101 50  0000 L CNN
F 1 "Conn_01x03_Female" H 2928 4010 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x03_P2.54mm_Vertical" H 2900 4075 50  0001 C CNN
F 3 "~" H 2900 4075 50  0001 C CNN
	1    2900 4075
	1    0    0    -1  
$EndComp
Text Notes 2550 3825 0    79   ~ 0
Write Permit\n
Text GLabel -575 3475 0    50   Input ~ 0
D2
Text GLabel -575 3375 0    50   Input ~ 0
MISO
Text GLabel -575 3275 0    50   Input ~ 0
MOSI
Text GLabel -575 3175 0    50   Input ~ 0
SCK
Text GLabel -575 3075 0    50   Input ~ 0
A5
Text GLabel -575 2975 0    50   Input ~ 0
A4
Text GLabel -575 2875 0    50   Input ~ 0
A3
Text GLabel -575 2775 0    50   Input ~ 0
A2
Text GLabel -575 2675 0    50   Input ~ 0
A1
Text GLabel -575 2575 0    50   Input ~ 0
A0
Text GLabel -575 2475 0    50   Input ~ 0
VHI
Text GLabel -575 2375 0    50   Input ~ 0
AREF
Wire Wire Line
	-950 2275 -950 2250
Wire Wire Line
	-575 2275 -950 2275
$Comp
L power:+3V3 #PWR0109
U 1 1 61B2A857
P -950 2250
F 0 "#PWR0109" H -950 2100 50  0001 C CNN
F 1 "+3V3" V -935 2378 50  0000 L CNN
F 2 "" H -950 2250 50  0001 C CNN
F 3 "" H -950 2250 50  0001 C CNN
	1    -950 2250
	1    0    0    -1  
$EndComp
Text GLabel -575 2175 0    50   Input ~ 0
~RESET
Text GLabel -1350 3450 0    50   Input ~ 0
RX_D0
Text GLabel -1350 3350 0    50   Input ~ 0
TX_D1
Text GLabel -1350 3250 0    50   Input ~ 0
SDA
Text GLabel -1350 3150 0    50   Input ~ 0
SCL
Text GLabel -1350 3050 0    50   Input ~ 0
D5_5V
Text GLabel -1350 2950 0    50   Input ~ 0
D7
Text GLabel -1350 2850 0    50   Input ~ 0
D9
Text GLabel -1350 2750 0    50   Input ~ 0
D10
Text GLabel -1350 2650 0    50   Input ~ 0
D11
Text GLabel -1350 2550 0    50   Input ~ 0
D12
Text GLabel -1350 2450 0    50   Input ~ 0
D13
Wire Wire Line
	-1350 2250 -1700 2250
Text GLabel -1350 2350 0    50   Input ~ 0
VBUS
$Comp
L power:GND #PWR0110
U 1 1 61AF489D
P -1700 2250
F 0 "#PWR0110" H -1700 2000 50  0001 C CNN
F 1 "GND" H -1695 2077 50  0000 C CNN
F 2 "" H -1700 2250 50  0001 C CNN
F 3 "" H -1700 2250 50  0001 C CNN
	1    -1700 2250
	1    0    0    -1  
$EndComp
Text GLabel -1350 2150 0    50   Input ~ 0
VBAT
$Comp
L Connector:Conn_01x14_Female J15
U 1 1 61AD4934
P -375 2875
F 0 "J15" H -483 1950 50  0000 C CNN
F 1 "Conn_01x14_Female" H -483 2041 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x14_P2.54mm_Vertical" H -375 2875 50  0001 C CNN
F 3 "~" H -375 2875 50  0001 C CNN
	1    -375 2875
	1    0    0    1   
$EndComp
$Comp
L Connector:Conn_01x14_Female J3
U 1 1 61AD28EB
P -1150 2750
F 0 "J3" H -1122 2726 50  0000 L CNN
F 1 "Conn_01x14_Female" V -1075 1850 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x14_P2.54mm_Vertical" H -1150 2750 50  0001 C CNN
F 3 "~" H -1150 2750 50  0001 C CNN
	1    -1150 2750
	1    0    0    -1  
$EndComp
Text Notes -1475 1700 0    79   ~ 0
Itsy Bitsy\n
Text GLabel 9650 1825 0    50   Input ~ 0
A0
Text GLabel 9650 1925 0    50   Input ~ 0
A1
Text GLabel 9650 2025 0    50   Input ~ 0
RX_D0
Text GLabel 9650 2125 0    50   Input ~ 0
TX_D1
Text GLabel 3925 2675 0    50   Input ~ 0
D7
Text GLabel 3925 2775 0    50   Input ~ 0
D2
Text Notes 2925 2775 0    39   ~ 0
pin 9 was D7; is now D6\npin 10 was D5; is now D2
Text GLabel 2700 4075 0    50   Input ~ 0
A3
$Comp
L Connector:Conn_01x13_Female J21
U 1 1 61B7A2AB
P 1450 1950
F 0 "J21" H 1478 1976 50  0000 L CNN
F 1 "Conn_01x13_Female" H 1478 1885 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x13_P2.54mm_Vertical" H 1450 1950 50  0001 C CNN
F 3 "~" H 1450 1950 50  0001 C CNN
	1    1450 1950
	1    0    0    -1  
$EndComp
Text Notes 1075 1150 0    79   ~ 0
Adafruit Think Ink 
$Comp
L power:GND #PWR0108
U 1 1 61C14C6B
P 7925 4975
F 0 "#PWR0108" H 7925 4725 50  0001 C CNN
F 1 "GND" H 7930 4802 50  0000 C CNN
F 2 "" H 7925 4975 50  0001 C CNN
F 3 "" H 7925 4975 50  0001 C CNN
	1    7925 4975
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR0112
U 1 1 61C14C75
P 8150 5075
F 0 "#PWR0112" H 8150 4925 50  0001 C CNN
F 1 "+3V3" H 8165 5248 50  0000 C CNN
F 2 "" H 8150 5075 50  0001 C CNN
F 3 "" H 8150 5075 50  0001 C CNN
	1    8150 5075
	1    0    0    -1  
$EndComp
Text GLabel 8325 5175 0    50   Input ~ 0
SDA
Text GLabel 8325 5275 0    50   Input ~ 0
SCL
Wire Wire Line
	8325 4975 7925 4975
Wire Wire Line
	8325 5075 8150 5075
$Comp
L Connector:Conn_01x04_Female J14
U 1 1 61C14C83
P 8525 5075
F 0 "J14" H 8553 5051 50  0000 L CNN
F 1 "Conn_01x04_Female" V 8700 4625 50  0000 L CNN
F 2 "Connector_JST:JST_SH_SM04B-SRSS-TB_1x04-1MP_P1.00mm_Horizontal" H 8525 5075 50  0001 C CNN
F 3 "~" H 8525 5075 50  0001 C CNN
	1    8525 5075
	1    0    0    -1  
$EndComp
Text Notes 7700 4725 0    79   ~ 0
QWIIC
$Comp
L Device:R R6
U 1 1 61C356AF
P 5250 6950
F 0 "R6" H 5320 6996 50  0000 L CNN
F 1 "4.7K" H 5320 6905 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" V 5180 6950 50  0001 C CNN
F 3 "~" H 5250 6950 50  0001 C CNN
	1    5250 6950
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR0113
U 1 1 61C356B9
P 5250 6800
F 0 "#PWR0113" H 5250 6650 50  0001 C CNN
F 1 "+3V3" H 5265 6973 50  0000 C CNN
F 2 "" H 5250 6800 50  0001 C CNN
F 3 "" H 5250 6800 50  0001 C CNN
	1    5250 6800
	1    0    0    -1  
$EndComp
Text Notes 5350 6550 0    79   ~ 0
BUTTON C
Wire Wire Line
	4750 7100 5250 7100
$Comp
L Switch:SW_Push SW3
U 1 1 61C356C5
P 5700 7100
F 0 "SW3" H 5700 7385 50  0000 C CNN
F 1 "SW_Push" H 5700 7294 50  0000 C CNN
F 2 "Button_Switch_THT:SW_Tactile_SPST_Angled_PTS645Vx58-2LFS" H 5700 7300 50  0001 C CNN
F 3 "~" H 5700 7300 50  0001 C CNN
	1    5700 7100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0114
U 1 1 61C356CF
P 5900 7100
F 0 "#PWR0114" H 5900 6850 50  0001 C CNN
F 1 "GND" H 5905 6927 50  0000 C CNN
F 2 "" H 5900 7100 50  0001 C CNN
F 3 "" H 5900 7100 50  0001 C CNN
	1    5900 7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 7100 5250 7100
Connection ~ 5250 7100
$Comp
L Switch:SW_Push_Dual SW4
U 1 1 61BD3A38
P 6475 3325
F 0 "SW4" H 6475 3610 50  0000 C CNN
F 1 "SW_Push_Dual" H 6475 3519 50  0000 C CNN
F 2 "Button_Switch_THT:SW_TH_Tactile_Omron_B3F-10xx" H 6475 3525 50  0001 C CNN
F 3 "~" H 6475 3525 50  0001 C CNN
	1    6475 3325
	1    0    0    -1  
$EndComp
Wire Wire Line
	6275 3325 6000 3325
Wire Wire Line
	6000 3325 6000 3025
Text GLabel 6000 3025 0    50   Input ~ 0
~RESET
Wire Wire Line
	6275 3325 6275 3200
Wire Wire Line
	6275 3200 6675 3200
Wire Wire Line
	6675 3200 6675 3325
Connection ~ 6275 3325
Wire Wire Line
	6675 3525 6675 3400
Wire Wire Line
	6675 3400 6275 3400
Wire Wire Line
	6275 3400 6275 3525
Wire Wire Line
	6675 3525 6675 3650
Connection ~ 6675 3525
$Comp
L Adafruit_AirLift_FeatherWing-eagle-import:GND #GND0101
U 1 1 61BE19AA
P 6675 3750
F 0 "#GND0101" H 6675 3750 50  0001 C CNN
F 1 "GND" H 6675 3629 59  0000 C CNN
F 2 "" H 6675 3750 50  0001 C CNN
F 3 "" H 6675 3750 50  0001 C CNN
	1    6675 3750
	1    0    0    -1  
$EndComp
Text Notes 6050 2875 0    79   ~ 0
EXT RESET
$Comp
L power:GND #PWR0115
U 1 1 61C6132E
P 10100 4500
F 0 "#PWR0115" H 10100 4250 50  0001 C CNN
F 1 "GND" H 10105 4327 50  0000 C CNN
F 2 "" H 10100 4500 50  0001 C CNN
F 3 "" H 10100 4500 50  0001 C CNN
	1    10100 4500
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR0116
U 1 1 61C61338
P 10325 4600
F 0 "#PWR0116" H 10325 4450 50  0001 C CNN
F 1 "+3V3" H 10340 4773 50  0000 C CNN
F 2 "" H 10325 4600 50  0001 C CNN
F 3 "" H 10325 4600 50  0001 C CNN
	1    10325 4600
	1    0    0    -1  
$EndComp
Text GLabel 10500 4700 0    50   Input ~ 0
SDA
Text GLabel 10500 4800 0    50   Input ~ 0
SCL
Wire Wire Line
	10500 4500 10100 4500
Wire Wire Line
	10500 4600 10325 4600
$Comp
L Connector:Conn_01x04_Female J13
U 1 1 61C61346
P 10700 4600
F 0 "J13" H 10728 4576 50  0000 L CNN
F 1 "Conn_01x04_Female" V 10875 4150 50  0000 L CNN
F 2 "Connector_JST:JST_SH_SM04B-SRSS-TB_1x04-1MP_P1.00mm_Horizontal" H 10700 4600 50  0001 C CNN
F 3 "~" H 10700 4600 50  0001 C CNN
	1    10700 4600
	1    0    0    -1  
$EndComp
Text Notes 9875 4250 0    79   ~ 0
QWIIC
$Comp
L power:GND #PWR02
U 1 1 6140B58B
P 1275 5150
F 0 "#PWR02" H 1275 4900 50  0001 C CNN
F 1 "GND" H 1280 4977 50  0000 C CNN
F 2 "" H 1275 5150 50  0001 C CNN
F 3 "" H 1275 5150 50  0001 C CNN
	1    1275 5150
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x08_Female J18
U 1 1 61BDE9C7
P 2675 1650
F 0 "J18" H 2703 1626 50  0000 L CNN
F 1 "Conn_01x08_Female" H 2703 1535 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 2675 1650 50  0001 C CNN
F 3 "~" H 2675 1650 50  0001 C CNN
	1    2675 1650
	1    0    0    -1  
$EndComp
Text Notes 2300 1200 0    79   ~ 0
Adafruit 128x128 oled\n
Text GLabel 1275 5050 0    50   Input ~ 0
VBAT_EXT
Text GLabel 3500 7375 0    50   Input ~ 0
VHI
$Comp
L Device:D D2
U 1 1 61BE095C
P 3650 7375
F 0 "D2" H 3650 7158 50  0000 C CNN
F 1 "D" H 3650 7249 50  0000 C CNN
F 2 "Diode_THT:D_5W_P12.70mm_Horizontal" H 3650 7375 50  0001 C CNN
F 3 "~" H 3650 7375 50  0001 C CNN
	1    3650 7375
	1    0    0    -1  
$EndComp
Text GLabel 3400 5975 2    50   Input ~ 0
VHI
Text Notes -3650 1800 0    79   ~ 0
lipo batt charger usb c\n(adafruit 4410)
Wire Wire Line
	-3550 2375 -3550 2400
Wire Wire Line
	-3125 2375 -3550 2375
Text GLabel -3125 2475 0    50   Input ~ 0
VBAT
$Comp
L power:GND #PWR0111
U 1 1 61BA3026
P -3550 2400
F 0 "#PWR0111" H -3550 2150 50  0001 C CNN
F 1 "GND" H -3545 2227 50  0000 C CNN
F 2 "" H -3550 2400 50  0001 C CNN
F 3 "" H -3550 2400 50  0001 C CNN
	1    -3550 2400
	1    0    0    -1  
$EndComp
Text GLabel -3125 2075 0    50   Input ~ 0
5V
$Comp
L Connector:Conn_01x05_Female J2
U 1 1 61B9E2DC
P -2925 2275
F 0 "J2" H -2897 2301 50  0000 L CNN
F 1 "Conn_01x05_Female" V -2775 1875 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x05_P2.54mm_Vertical" H -2925 2275 50  0001 C CNN
F 3 "~" H -2925 2275 50  0001 C CNN
	1    -2925 2275
	1    0    0    -1  
$EndComp
$EndSCHEMATC
