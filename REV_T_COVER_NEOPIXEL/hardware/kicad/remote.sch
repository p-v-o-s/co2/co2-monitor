EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Mechanical:MountingHole_Pad H1
U 1 1 614A1FDA
P 7075 5825
F 0 "H1" H 7175 5874 50  0000 L CNN
F 1 "MountingHole_Pad" H 7175 5783 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5_Pad" H 7075 5825 50  0001 C CNN
F 3 "~" H 7075 5825 50  0001 C CNN
	1    7075 5825
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H2
U 1 1 614A5606
P 7325 6150
F 0 "H2" H 7425 6199 50  0000 L CNN
F 1 "MountingHole_Pad" H 7425 6108 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5_Pad" H 7325 6150 50  0001 C CNN
F 3 "~" H 7325 6150 50  0001 C CNN
	1    7325 6150
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H3
U 1 1 614A5CC9
P 8550 5775
F 0 "H3" H 8650 5824 50  0000 L CNN
F 1 "MountingHole_Pad" H 8650 5733 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5_Pad" H 8550 5775 50  0001 C CNN
F 3 "~" H 8550 5775 50  0001 C CNN
	1    8550 5775
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H4
U 1 1 614A5FBB
P 8550 6125
F 0 "H4" H 8650 6174 50  0000 L CNN
F 1 "MountingHole_Pad" H 8650 6083 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5_Pad" H 8550 6125 50  0001 C CNN
F 3 "~" H 8550 6125 50  0001 C CNN
	1    8550 6125
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H5
U 1 1 614E3462
P 9725 5800
F 0 "H5" H 9825 5849 50  0000 L CNN
F 1 "MountingHole_Pad" H 9825 5758 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5_Pad" H 9725 5800 50  0001 C CNN
F 3 "~" H 9725 5800 50  0001 C CNN
	1    9725 5800
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H6
U 1 1 614E3468
P 9725 6150
F 0 "H6" H 9825 6199 50  0000 L CNN
F 1 "MountingHole_Pad" H 9825 6108 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5_Pad" H 9725 6150 50  0001 C CNN
F 3 "~" H 9725 6150 50  0001 C CNN
	1    9725 6150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 615C33CE
P 7075 5925
F 0 "#PWR0101" H 7075 5675 50  0001 C CNN
F 1 "GND" H 7080 5752 50  0000 C CNN
F 2 "" H 7075 5925 50  0001 C CNN
F 3 "" H 7075 5925 50  0001 C CNN
	1    7075 5925
	0    1    1    0   
$EndComp
$Comp
L power:+3V3 #PWR0102
U 1 1 615C451F
P 7325 6250
F 0 "#PWR0102" H 7325 6100 50  0001 C CNN
F 1 "+3V3" H 7340 6423 50  0000 C CNN
F 2 "" H 7325 6250 50  0001 C CNN
F 3 "" H 7325 6250 50  0001 C CNN
	1    7325 6250
	0    -1   -1   0   
$EndComp
Text GLabel 8550 5875 0    50   Input ~ 0
SCL
Text GLabel 8550 6225 0    50   Input ~ 0
SDA
Text GLabel 9725 5900 0    50   Input ~ 0
A3
Text GLabel 9725 6250 0    50   Input ~ 0
D5
$Comp
L Connector:Conn_01x04_Female J1
U 1 1 614ECAD9
P 7600 4675
F 0 "J1" H 7628 4651 50  0000 L CNN
F 1 "Conn_01x04_Female" H 7628 4560 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Vertical" H 7600 4675 50  0001 C CNN
F 3 "~" H 7600 4675 50  0001 C CNN
	1    7600 4675
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 614ED3DE
P 7400 4575
F 0 "#PWR0103" H 7400 4325 50  0001 C CNN
F 1 "GND" H 7405 4402 50  0000 C CNN
F 2 "" H 7400 4575 50  0001 C CNN
F 3 "" H 7400 4575 50  0001 C CNN
	1    7400 4575
	0    1    1    0   
$EndComp
$Comp
L power:+3V3 #PWR0104
U 1 1 614EDD86
P 7175 4675
F 0 "#PWR0104" H 7175 4525 50  0001 C CNN
F 1 "+3V3" H 7190 4848 50  0000 C CNN
F 2 "" H 7175 4675 50  0001 C CNN
F 3 "" H 7175 4675 50  0001 C CNN
	1    7175 4675
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7400 4675 7175 4675
Text GLabel 7400 4775 0    50   Input ~ 0
SCL
Text GLabel 7400 4875 0    50   Input ~ 0
SDA
$Comp
L Connector:Conn_01x04_Female J2
U 1 1 614F3DE0
P 5500 4050
F 0 "J2" H 5528 4026 50  0000 L CNN
F 1 "Conn_01x04_Female" H 5528 3935 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Vertical" H 5500 4050 50  0001 C CNN
F 3 "~" H 5500 4050 50  0001 C CNN
	1    5500 4050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 614F3DE6
P 5300 3950
F 0 "#PWR0105" H 5300 3700 50  0001 C CNN
F 1 "GND" H 5305 3777 50  0000 C CNN
F 2 "" H 5300 3950 50  0001 C CNN
F 3 "" H 5300 3950 50  0001 C CNN
	1    5300 3950
	0    1    1    0   
$EndComp
$Comp
L power:+3V3 #PWR0106
U 1 1 614F3DEC
P 5075 4050
F 0 "#PWR0106" H 5075 3900 50  0001 C CNN
F 1 "+3V3" H 5090 4223 50  0000 C CNN
F 2 "" H 5075 4050 50  0001 C CNN
F 3 "" H 5075 4050 50  0001 C CNN
	1    5075 4050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5300 4050 5075 4050
Text GLabel 5300 4150 0    50   Input ~ 0
SCL
Text GLabel 5300 4250 0    50   Input ~ 0
SDA
$EndSCHEMATC
