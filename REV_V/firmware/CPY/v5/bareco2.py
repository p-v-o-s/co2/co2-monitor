import board
import busio
import digitalio
from digitalio import DigitalInOut
import adafruit_scd30
import time
# Get wifi details and more from a secrets.py file

# fet
q1 = digitalio.DigitalInOut(board.D9)
q1.direction = digitalio.Direction.OUTPUT
q1.value = True

exception_count = 0
sample_recorded_count = 0

time_init=time.monotonic()

interval_seconds = 2
time.sleep(3)

scd = adafruit_scd30.SCD30(board.I2C())

while True:

    if (scd.data_available):
        try:
            if (scd.CO2>1):
                # print("Data Available!")
                print("CO2: %d PPM" % scd.CO2)
                time.sleep(2)
        except Exception as e:
            print("*** Exception: " + str(e))
            exception_count += 1
            # break
