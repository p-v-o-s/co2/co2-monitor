EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "PVOS CO2 Monitor"
Date ""
Rev "V"
Comp ""
Comment1 "http://pvos.org/co2/rev_v"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Mechanical:MountingHole H5
U 1 1 614E3462
P 9700 3825
F 0 "H5" H 9800 3874 50  0000 L CNN
F 1 "MountingHole" H 9800 3783 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 9700 3825 50  0001 C CNN
F 3 "~" H 9700 3825 50  0001 C CNN
	1    9700 3825
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H6
U 1 1 614E3468
P 9700 4175
F 0 "H6" H 9800 4224 50  0000 L CNN
F 1 "MountingHole" H 9800 4133 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 9700 4175 50  0001 C CNN
F 3 "~" H 9700 4175 50  0001 C CNN
	1    9700 4175
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_AirLift_FeatherWing-eagle-import:MOUNTINGHOLE2.5 U4
U 1 1 61CC19BA
P 5000 3925
F 0 "U4" H 5130 3925 50  0000 L CNN
F 1 "MOUNTINGHOLE2.5" H 5000 3925 50  0001 C CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 5000 3925 50  0001 C CNN
F 3 "" H 5000 3925 50  0001 C CNN
	1    5000 3925
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_AirLift_FeatherWing-eagle-import:MOUNTINGHOLE2.5 U1
U 1 1 61CC1D62
P 5375 3750
F 0 "U1" H 5505 3750 50  0000 L CNN
F 1 "MOUNTINGHOLE2.5" H 5375 3750 50  0001 C CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 5375 3750 50  0001 C CNN
F 3 "" H 5375 3750 50  0001 C CNN
	1    5375 3750
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_AirLift_FeatherWing-eagle-import:MOUNTINGHOLE2.5 U2
U 1 1 61CC2056
P 5375 4000
F 0 "U2" H 5505 4000 50  0000 L CNN
F 1 "MOUNTINGHOLE2.5" H 5375 4000 50  0001 C CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 5375 4000 50  0001 C CNN
F 3 "" H 5375 4000 50  0001 C CNN
	1    5375 4000
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_AirLift_FeatherWing-eagle-import:MOUNTINGHOLE2.5 U3
U 1 1 61CC2246
P 5375 4250
F 0 "U3" H 5505 4250 50  0000 L CNN
F 1 "MOUNTINGHOLE2.5" H 5375 4250 50  0001 C CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 5375 4250 50  0001 C CNN
F 3 "" H 5375 4250 50  0001 C CNN
	1    5375 4250
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_AirLift_FeatherWing-eagle-import:MOUNTINGHOLE2.5 U5
U 1 1 61D667E0
P 3725 2700
F 0 "U5" H 3855 2700 50  0000 L CNN
F 1 "MOUNTINGHOLE2.5" H 3725 2700 50  0001 C CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 3725 2700 50  0001 C CNN
F 3 "" H 3725 2700 50  0001 C CNN
	1    3725 2700
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_AirLift_FeatherWing-eagle-import:MOUNTINGHOLE2.5 U6
U 1 1 61D667EA
P 4225 2650
F 0 "U6" H 4355 2650 50  0000 L CNN
F 1 "MOUNTINGHOLE2.5" H 4225 2650 50  0001 C CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 4225 2650 50  0001 C CNN
F 3 "" H 4225 2650 50  0001 C CNN
	1    4225 2650
	1    0    0    -1  
$EndComp
$EndSCHEMATC
